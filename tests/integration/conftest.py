"""Package's fixtures."""

import datetime

import pytest
from sqlalchemy import create_engine
from sqlalchemy.schema import CreateSchema

from notifications_routing.app import configure_logging
from notifications_routing.config import Config, load_config
from notifications_routing.data_source.postgres.postgres_data_source import (
    Channel,
    Device,
    Group,
    Mute,
    Notification,
    PostgresDataSource,
    Preference,
    User,
)


@pytest.fixture(scope="module")
def config():
    """Set up config."""
    config = load_config()
    yield config


@pytest.fixture(scope="module")
def appctx(config):
    """Set up app context."""
    configure_logging(config)


@pytest.fixture(scope="module")
def data_source():
    """Set up data source."""
    engine = create_engine(Config.SQLALCHEMY_DATABASE_URI)
    if not engine.dialect.has_schema(engine, Config.DB_SCHEMA):
        engine.execute(CreateSchema(Config.DB_SCHEMA))

    PostgresDataSource.Base.prepare(engine)
    PostgresDataSource.Base.metadata.create_all(engine)

    db = PostgresDataSource()
    yield db

    PostgresDataSource.Base.metadata.drop_all(bind=engine)


@pytest.fixture(scope="function")
def session(data_source):
    """Return  database session for test."""
    with data_source.session() as session:
        yield session


@pytest.fixture(scope="function")
def user(session):
    """Insert user to db."""
    user = User()
    user.id = "16fd2706-8baf-433b-82eb-8c7fada847da"
    user.username = "testuser"
    user.email = "testuser@cern.ch"
    user.enabled = True
    user.created = datetime.datetime(2020, 1, 1)
    user.preferences = []
    user.lastLogin = datetime.datetime(2020, 1, 1)

    session.add(user)
    session.commit()

    yield user

    session.delete(user)
    session.commit()


@pytest.fixture(scope="function")
def unsubscribed_user(session):
    """Insert another user to db."""
    user = User()
    user.id = "17fd2706-8baf-433b-82eb-8c7fada847da"
    user.username = "unsubscribed_user"
    user.email = "unsubscribed_user@cern.ch"
    user.enabled = False
    user.created = datetime.datetime(2020, 1, 1)
    user.preferences = []
    user.lastLogin = datetime.datetime(2020, 2, 2)

    session.add(user)
    session.commit()

    yield user

    session.delete(user)
    session.commit()


@pytest.fixture(scope="function")
def group(session):
    """Insert group to db."""
    group = Group()
    group.id = "186d8dfc-2774-43a8-91b5-a887fcb6ba4a"
    group.groupIdentifier = "test-group"

    session.add(group)
    session.commit()

    yield group

    session.delete(group)
    session.commit()


@pytest.fixture(scope="function")
def channel(session, group, user, unsubscribed_user):
    """Insert channel to db."""
    channel = Channel()
    channel.id = "c3ccc15b-298f-4dc7-877f-2c8970331caf"
    channel.slug = "test-channel"
    channel.name = "Test Channel"
    channel.groups = [group]
    channel.members = [user, unsubscribed_user]
    channel.deleteDate = None
    channel.unsubscribed = [unsubscribed_user]

    session.add(channel)
    session.commit()

    yield channel

    channel.unsubscribed = []
    channel.members = []
    channel.groups = []
    session.add(channel)
    session.commit()

    session.delete(channel)
    session.commit()


@pytest.fixture(scope="function")
def target_notification(session, group, user, unsubscribed_user):
    """Insert channel to db."""
    target_notification = Notification()
    target_notification.id = "c3ccc15b-298f-4dc7-877f-2c8970331caf"
    target_notification.target_groups = [group]
    target_notification.target_users = [user, unsubscribed_user]

    session.add(target_notification)
    session.commit()

    yield target_notification

    target_notification.target_groups = []
    target_notification.target_users = []
    session.add(target_notification)
    session.commit()

    session.delete(target_notification)
    session.commit()


@pytest.fixture(scope="function")
def device(session):
    """Insert device to db."""
    device = Device()
    device.id = "d4e15d11-a56e-4568-9e9f-497110426a72"
    device.name = "testuser@cern.ch"
    device.userId = "16fd2706-8baf-433b-82eb-8c7fada847da"
    device.info = "Default"
    device.type = "EMAIL"
    device.subType = None
    device.token = None

    session.add(device)
    session.commit()

    yield device

    session.delete(device)
    session.commit()


@pytest.fixture(scope="function")
def preference(session):
    """Insert preference to db."""
    preference = Preference()
    preference.type = "DAILY"
    preference.name = "Daily preference"
    preference.userId = "16fd2706-8baf-433b-82eb-8c7fada847da"
    preference.targetId = "c3ccc15b-298f-4dc7-877f-2c8970331caf"
    preference.notificationPriority = "LOW"
    preference.rangeStart = None
    preference.rangeEnd = None
    preference.scheduledTime = datetime.time(13, 0)

    session.add(preference)
    session.commit()

    yield preference

    session.delete(preference)
    session.commit()


@pytest.fixture(scope="function")
def mute_permanent(session):
    """Insert mute to db."""
    mute = Mute()
    mute.type = "PERMANENT"
    mute.userId = "16fd2706-8baf-433b-82eb-8c7fada847da"
    mute.targetId = "c3ccc15b-298f-4dc7-877f-2c8970331caf"
    mute.start = None
    mute.end = None

    session.add(mute)
    session.commit()

    yield mute

    session.delete(mute)
    session.commit()


@pytest.fixture(scope="function")
def mute_range(session):
    """Insert mute to db."""
    mute = Mute()
    mute.type = "RANGE"
    mute.userId = "16fd2706-8baf-433b-82eb-8c7fada847da"
    mute.targetId = "c3ccc15b-298f-4dc7-877f-2c8970331caf"
    mute.start = datetime.datetime(2012, 4, 1, 1, 0)
    mute.end = datetime.datetime(3000, 4, 1, 1, 0)

    session.add(mute)
    session.commit()

    yield mute

    session.delete(mute)
    session.commit()
