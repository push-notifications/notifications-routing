"""Integration Tests for PostgresDataSource."""

import datetime
from uuid import UUID


def test_get_notification_target_users(appctx, data_source, target_notification):
    """Test get notification target users."""
    users = data_source.get_target_users(target_notification.id)
    assert users

    expected_data = [
        {
            "user_id": UUID("16fd2706-8baf-433b-82eb-8c7fada847da"),
            "username": "testuser",
            "email": "testuser@cern.ch",
            "last_login": datetime.datetime(2020, 1, 1),
        },
        {
            "user_id": UUID("17fd2706-8baf-433b-82eb-8c7fada847da"),
            "username": "unsubscribed_user",
            "email": "unsubscribed_user@cern.ch",
            "last_login": datetime.datetime(2020, 2, 2),
        },
    ]

    assert len(users) == len(expected_data)
    for user in users:
        assert user in expected_data


def test_get_notification_target_groups(appctx, data_source, target_notification):
    """Test get target groups."""
    groups = data_source.get_target_groups(target_notification.id)
    assert groups
    assert len(groups) == 1

    channel_group = groups[0]
    assert str(channel_group) == "186d8dfc-2774-43a8-91b5-a887fcb6ba4a"


def test_get_channel_users(appctx, data_source, channel):
    """Test get channel users."""
    channel_users = data_source.get_channel_subscribed_users(channel.id)
    assert channel_users
    assert len(channel_users) == 1

    channel_user = channel_users[0]
    assert str(channel_user["user_id"]) == "16fd2706-8baf-433b-82eb-8c7fada847da"
    assert channel_user["username"] == "testuser"
    assert channel_user["email"] == "testuser@cern.ch"
    assert channel_user["last_login"] == datetime.datetime(2020, 1, 1)


def test_get_channel_unsubscribed_users(appctx, data_source, channel):
    """Test get channel unsubscribed users."""
    assert data_source.get_channel_unsubscribed_users(channel.id) == ["unsubscribed_user"]


def test_get_channel_groups(appctx, data_source, channel):
    """Test get channel groups."""
    channel_groups = data_source.get_channel_groups(channel.id)
    assert channel_groups
    assert len(channel_groups) == 1

    channel_group = channel_groups[0]
    assert str(channel_group) == "186d8dfc-2774-43a8-91b5-a887fcb6ba4a"


def test_get_user_devices(appctx, data_source, user, device):
    """Test get user devices."""
    user_devices = data_source.get_user_devices(user.id)
    assert user_devices
    assert user_devices["email"] == user.email
    assert len(user_devices["devices"]) == 1

    user_device = user_devices["devices"][0]
    assert str(user_device.id) == "d4e15d11-a56e-4568-9e9f-497110426a72"
    assert user_device.name == "testuser@cern.ch"
    assert str(user_device.userId) == "16fd2706-8baf-433b-82eb-8c7fada847da"
    assert user_device.info == "Default"
    assert user_device.type == "EMAIL"
    assert user_device.subType is None
    assert user_device.token is None


def test_get_user_preferences(appctx, data_source, user, preference):
    """Test get user preferences."""
    user_preferences = data_source.get_user_preferences(user.id, "c3ccc15b-298f-4dc7-877f-2c8970331caf")
    assert user_preferences
    assert user_preferences["email"] == user.email

    assert len(user_preferences["preferences"]) == 1
    user_preference = user_preferences["preferences"][0]
    assert user_preference.type == "DAILY"
    assert user_preference.name == "Daily preference"
    assert str(user_preference.userId) == user.id
    assert user_preference.targetId == "c3ccc15b-298f-4dc7-877f-2c8970331caf"
    assert user_preference.notificationPriority == "LOW"
    assert user_preference.rangeStart is None
    assert user_preference.rangeEnd is None
    assert user_preference.scheduledTime == datetime.time(13, 0)


def test_get_system_user(appctx, data_source, user):
    """Test get system user."""
    system_user = data_source.get_system_user(user.username)
    assert str(system_user["user_id"]) == user.id
    assert system_user["username"] == user.username
    assert system_user["email"] == user.email
    assert system_user["last_login"] == user.lastLogin


def test_is_user_mute_active_permanent(appctx, data_source, user, mute_permanent):
    """Test if user permanent mute is active."""
    assert data_source.is_user_mute_active(user.id, "c3ccc15b-298f-4dc7-877f-2c8970331caf") is True


def test_is_user_mute_active_range(appctx, data_source, user, mute_range):
    """Test if user range mute is active."""
    assert data_source.is_user_mute_active(user.id, "c3ccc15b-298f-4dc7-877f-2c8970331caf") is True
