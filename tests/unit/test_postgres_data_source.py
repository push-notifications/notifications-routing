"""Unit Tests for PostgresDataSource."""

import datetime
from unittest import mock
from unittest.mock import ANY, MagicMock

import pytest
from pytest import raises

from notifications_routing.data_source.postgres.postgres_data_source import Channel, Group, PostgresDataSource, User
from notifications_routing.exceptions import NotFoundDataSourceError


@pytest.fixture(scope="function")
def db_mock():
    """Private access record."""
    with mock.patch.object(PostgresDataSource, "__init__", return_value=None):
        mock_db = PostgresDataSource()
        mock_db.session = MagicMock()

        return mock_db


@pytest.fixture(scope="function")
def query_mock(db_mock):
    """Query mock."""
    query = db_mock.session.return_value.__enter__.return_value.query.return_value = MagicMock()

    return query


@pytest.fixture(scope="function")
def channel():
    """Channel object."""
    return Channel(
        id="c3ccc15b-298f-4dc7-877f-2c8970331caf",
        slug="test-channel",
        name="Test Channel",
        groups=[
            Group(
                id="186d8dfc-2774-43a8-91b5-a887fcb6ba4a",
                groupIdentifier="test-group",
            )
        ],
        members=[
            User(
                id="3eacfc35-42bd-49e8-9f2c-1e552c447ff3",
                username="testuser",
                email="testuser@cern.ch",
                enabled=True,
                created=datetime.datetime(2012, 1, 1, 1, 0),
                preferences=[],
                lastLogin=datetime.datetime(2012, 2, 1, 1, 0),
            )
        ],
        deleteDate=None,
        unsubscribed=[
            User(
                id="6aecfc35-42bd-49e8-9f2c-1e552c447ff3",
                username="unsubscribeduser",
                email="unsubscribeduser@cern.ch",
                enabled=True,
                created=datetime.datetime(2012, 1, 1, 1, 0),
            )
        ],
    )


@pytest.fixture(scope="function")
def user():
    """User object."""
    return User(
        id="3eacfc35-42bd-49e8-9f2c-1e552c447ff3",
        username="testuser",
        email="testuser@cern.ch",
        enabled=True,
        created=datetime.datetime(2012, 1, 1, 1, 0),
        preferences=[],
        lastLogin=datetime.datetime(2012, 2, 1, 1, 0),
    )


@mock.patch.object(PostgresDataSource, "_PostgresDataSource__get_scalar")
def test_get_channel_users_not_found_datasource_error(mock_get_scalar, db_mock):
    """Test that NotFoundDataSourceError is raised when get_scalar returns None."""
    with raises(NotFoundDataSourceError):
        mock_get_scalar.return_value = None
        db_mock.get_channel_subscribed_users("c3ccc15b-298f-4dc7-877f-2c8970331caf")
        mock_get_scalar.assert_called_once_with(
            ANY, Channel, id="c3ccc15b-298f-4dc7-877f-2c8970331caf", deleteDate=None
        )


@mock.patch.object(PostgresDataSource, "_PostgresDataSource__get_scalar")
def test_get_channel_users(mock_get_scalar, db_mock, channel):
    """Test that channel users are returned."""
    mock_get_scalar.return_value = channel
    assert db_mock.get_channel_subscribed_users("c3ccc15b-298f-4dc7-877f-2c8970331caf") == [
        {
            "user_id": "3eacfc35-42bd-49e8-9f2c-1e552c447ff3",
            "username": "testuser",
            "email": "testuser@cern.ch",
            "last_login": datetime.datetime(2012, 2, 1, 1, 0),
        }
    ]
    mock_get_scalar.assert_called_once_with(ANY, Channel, id="c3ccc15b-298f-4dc7-877f-2c8970331caf", deleteDate=None)


@mock.patch.object(PostgresDataSource, "_PostgresDataSource__get_scalar")
def test_get_channel_groups_not_gound_datasource_error(mock_get_scalar, db_mock):
    """Test that NotFoundDataSourceError is raised when get_scalar returns None."""
    with raises(NotFoundDataSourceError):
        mock_get_scalar.return_value = None
        db_mock.get_channel_groups("c3ccc15b-298f-4dc7-877f-2c8970331caf")


@mock.patch.object(PostgresDataSource, "_PostgresDataSource__get_scalar")
def test_get_channel_groups_(mock_get_scalar, db_mock, channel):
    """Test that channel groups are returned."""
    mock_get_scalar.return_value = channel
    assert db_mock.get_channel_groups("c3ccc15b-298f-4dc7-877f-2c8970331caf") == [
        "186d8dfc-2774-43a8-91b5-a887fcb6ba4a"
    ]
    mock_get_scalar.assert_called_once_with(ANY, Channel, id="c3ccc15b-298f-4dc7-877f-2c8970331caf", deleteDate=None)


@mock.patch.object(PostgresDataSource, "_PostgresDataSource__get_scalar")
def test_get_channel_unsubscribed_users_(mock_get_scalar, db_mock, channel):
    """Test that channel unsubscribed users are returned."""
    mock_get_scalar.return_value = channel
    assert db_mock.get_channel_unsubscribed_users("c3ccc15b-298f-4dc7-877f-2c8970331caf") == ["unsubscribeduser"]
    mock_get_scalar.assert_called_once_with(ANY, Channel, id="c3ccc15b-298f-4dc7-877f-2c8970331caf", deleteDate=None)


@mock.patch("notifications_routing.data_source.postgres.postgres_data_source.get_group_users_api")
def test_get_group_users(mock_get_group_users_api, db_mock):
    """Test get group users."""
    mock_get_group_users_api.return_value = [
        {"username": "testuser", "email": "testuser@cern.ch"},
        {"username": "unconfirmedEmail@mail.ch", "email": "unconfirmedEmail@mail.ch"},
    ]

    assert db_mock.get_group_users("186d8dfc-2774-43a8-91b5-a887fcb6ba4a") == [
        {"username": "testuser", "email": "testuser@cern.ch"},
        {"username": "unconfirmedEmail@mail.ch", "email": "unconfirmedEmail@mail.ch"},
    ]
    mock_get_group_users_api.assert_called_once_with("186d8dfc-2774-43a8-91b5-a887fcb6ba4a")


@mock.patch.object(PostgresDataSource, "_PostgresDataSource__get_scalar")
def test_get_user_preferences_not_found_datasource_error(mock_get_scalar, db_mock):
    """Test that NotFoundDataSourceError is raised when get_scalar returns None."""
    with raises(NotFoundDataSourceError):
        mock_get_scalar.return_value = None
        db_mock.get_user_preferences("3eacfc35-42bd-49e8-9f2c-1e552c447ff3", "c3ccc15b-298f-4dc7-877f-2c8970331caf")


@mock.patch("notifications_routing.data_source.postgres.postgres_data_source.Preference.disabledChannels")
@mock.patch.object(PostgresDataSource, "_PostgresDataSource__get_scalar")
def test_get_user_preferences(mock_get_scalar, mock_preference, db_mock, query_mock, user):
    """Test get user preferences."""
    mock_get_scalar.return_value = user
    query_mock.filter.return_value.all.return_value = []
    assert db_mock.get_user_preferences(
        "3eacfc35-42bd-49e8-9f2c-1e552c447ff3", "c3ccc15b-298f-4dc7-877f-2c8970331caf"
    ) == {"email": "testuser@cern.ch", "preferences": []}
    mock_get_scalar.assert_called_once_with(ANY, User, id="3eacfc35-42bd-49e8-9f2c-1e552c447ff3")


@mock.patch.object(PostgresDataSource, "_PostgresDataSource__get_scalar")
def test_is_user_mute_active_false(mock_get_scalar, query_mock, db_mock, user):
    """Test that is_user_mute_active returns nothing if no mute is present."""
    mock_get_scalar.return_value = user
    query_mock.scalar.return_value = False

    assert (
        db_mock.is_user_mute_active("3eacfc35-42bd-49e8-9f2c-1e552c447ff3", "c3ccc15b-298f-4dc7-877f-2c8970331caf")
        is False
    )


@mock.patch.object(PostgresDataSource, "_PostgresDataSource__get_scalar")
def test_is_user_mute_active_true(mock_get_scalar, db_mock, query_mock, user):
    """Test that is_user_mute_active returns true if mute is present."""
    mock_get_scalar.return_value = user
    query_mock.scalar.return_value = True

    assert (
        db_mock.is_user_mute_active("3eacfc35-42bd-49e8-9f2c-1e552c447ff3", "c3ccc15b-298f-4dc7-877f-2c8970331caf")
        is True
    )


@mock.patch.object(PostgresDataSource, "_PostgresDataSource__get_scalar")
def test_get_user_devices_not_found_datasource_error(mock_get_scalar, db_mock):
    """Test that NotFoundDataSourceError is raised when get_scalar returns None."""
    with raises(NotFoundDataSourceError):
        mock_get_scalar.return_value = None
        db_mock.get_user_devices("3eacfc35-42bd-49e8-9f2c-1e552c447ff3")


@mock.patch.object(PostgresDataSource, "_PostgresDataSource__get_scalar")
def test_get_user_devices(mock_get_scalar, db_mock, query_mock, user):
    """Test get user devices."""
    mock_get_scalar.return_value = user
    query_mock.filter.return_value.all.return_value = []
    assert db_mock.get_user_devices("3eacfc35-42bd-49e8-9f2c-1e552c447ff3") == {
        "email": "testuser@cern.ch",
        "devices": [],
    }
    mock_get_scalar.assert_called_once_with(ANY, User, id="3eacfc35-42bd-49e8-9f2c-1e552c447ff3")


@mock.patch.object(PostgresDataSource, "_PostgresDataSource__get_scalar")
def test_get_system_user_not_found_datasource_error(mock_get_scalar, db_mock):
    """Test that NotFoundDataSourceError is raised when get_scalar returns None."""
    with raises(NotFoundDataSourceError):
        mock_get_scalar.return_value = None
        db_mock.get_user_devices("testuser")


@mock.patch.object(PostgresDataSource, "_PostgresDataSource__get_scalar")
def test_get_system_user(mock_get_scalar, db_mock, user):
    """Test get system user."""
    mock_get_scalar.return_value = user
    assert db_mock.get_system_user("testuser") == {
        "user_id": "3eacfc35-42bd-49e8-9f2c-1e552c447ff3",
        "username": "testuser",
        "email": "testuser@cern.ch",
        "last_login": datetime.datetime(2012, 2, 1, 1, 0),
    }
    mock_get_scalar.assert_called_once_with(ANY, User, username="testuser")
