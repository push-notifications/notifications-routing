"""Unit Tests for Router."""

from unittest import mock
from unittest.mock import ANY

from notifications_routing.data_source.postgres.postgres_data_source import User
from notifications_routing.exceptions import NotFoundDataSourceError
from notifications_routing.router import Router


def test_get_channel_users_one_group_without_no_system_users(
    router_mock, system_user_1, system_user_2, group_user_1, group_user_2
):
    """Test get channel users for one group without no system users."""
    router_mock.data_source.get_channel_subscribed_users.return_value = [system_user_1, system_user_2]
    router_mock.data_source.get_channel_groups.return_value = [
        {"group_id": "186d8dfc-2774-43a8-91b5-a887fcb6ba4a"},
    ]
    router_mock.data_source.get_group_users.return_value = [group_user_1, group_user_2]
    router_mock.data_source.get_system_user.side_effect = [system_user_1, system_user_2]

    assert router_mock.get_channel_users(ANY, "c3ccc15b-298f-4dc7-877f-2c8970331caf") == [system_user_1, system_user_2]


def test_get_channel_users_one_group_with_no_system_user(router_mock, no_system_user_1):
    """Test get channel users for one group with a no system user."""
    router_mock.data_source.get_channel_subscribed_users.return_value = []
    router_mock.data_source.get_channel_groups.return_value = ["186d8dfc-2774-43a8-91b5-a887fcb6ba4a"]
    router_mock.data_source.get_group_users.return_value = [no_system_user_1]
    router_mock.data_source.get_system_user.side_effect = NotFoundDataSourceError(User, username="nosystemuser")

    assert router_mock.get_channel_users(ANY, "c3ccc15b-298f-4dc7-877f-2c8970331caf") == [no_system_user_1]


def test_get_channel_users_multiple_groups_with_overlapping_users(
    router_mock, system_user_1, system_user_2, system_user_3, group_user_1, group_user_2, group_user_3
):
    """Test get channel users for multiple groups with overlapping users."""
    router_mock.data_source.get_channel_subscribed_users.return_value = [system_user_2, system_user_3]
    router_mock.data_source.get_channel_groups.return_value = [
        "186d8dfc-2774-43a8-91b5-a887fcb6ba4a",
        "ffff6789-2774-43a8-91b5-a887fcb6ba4a",
    ]
    router_mock.data_source.get_group_users.side_effect = [[group_user_1, group_user_2], [group_user_1, group_user_3]]
    router_mock.data_source.get_system_user.side_effect = [system_user_1]

    assert router_mock.get_channel_users(ANY, "c3ccc15b-298f-4dc7-877f-2c8970331caf") == [
        system_user_2,
        system_user_3,
        system_user_1,
    ]


def test_get_channel_no_users_multiple_groups_with_overlapping_users(
    router_mock, system_user_1, group_user_1, group_user_2, group_user_3
):
    """Test get channel users w/out users for multiple groups with overlapping users."""
    router_mock.data_source.get_channel_subscribed_users.return_value = []
    router_mock.data_source.get_channel_groups.return_value = [
        "186d8dfc-2774-43a8-91b5-a887fcb6ba4a",
        "ffff6789-2774-43a8-91b5-a887fcb6ba4a",
    ]
    router_mock.data_source.get_group_users.side_effect = [[group_user_1, group_user_2], [group_user_1, group_user_3]]
    router_mock.data_source.get_system_user.side_effect = [
        system_user_1,
        NotFoundDataSourceError(""),
        NotFoundDataSourceError(""),
    ]

    assert router_mock.get_channel_users(ANY, "c3ccc15b-298f-4dc7-877f-2c8970331caf") == [
        system_user_1,
        group_user_2,
        group_user_3,
    ]


def test_get_channel_users_group_with_system_and_no_system_users(
    router_mock, system_user_1, system_user_2, no_system_user_1, no_system_user_2, group_user_1, group_user_2
):
    """Test get channel users for groups with system and no system users."""
    router_mock.data_source.get_channel_subscribed_users.return_value = [system_user_1, system_user_2]
    router_mock.data_source.get_channel_groups.return_value = [
        "186d8dfc-2774-43a8-91b5-a887fcb6ba4a",
    ]
    router_mock.data_source.get_group_users.return_value = [
        group_user_1,
        group_user_2,
        no_system_user_1,
        no_system_user_2,
    ]
    router_mock.data_source.get_system_user.side_effect = [
        NotFoundDataSourceError(User, username="nosystemuser1"),
        NotFoundDataSourceError(User, username="nosystemuser2"),
    ]
    assert router_mock.get_channel_users(ANY, "c3ccc15b-298f-4dc7-877f-2c8970331caf") == [
        system_user_1,
        system_user_2,
        no_system_user_1,
        no_system_user_2,
    ]


def test_get_channel_users_no_users(router_mock):
    """Test get channel users for no users."""
    router_mock.data_source.get_channel_subscribed_users.return_value = []
    router_mock.data_source.get_channel_groups.return_value = [
        "186d8dfc-2774-43a8-91b5-a887fcb6ba4a",
    ]
    router_mock.data_source.get_group_users.return_value = []
    router_mock.data_source.get_system_user.assert_not_called()
    assert router_mock.get_channel_users(ANY, "c3ccc15b-298f-4dc7-877f-2c8970331caf") == []


def test_get_channel_users_no_groups(router_mock, system_user_1, system_user_2):
    """Test get channel users for no groups."""
    router_mock.data_source.get_channel_subscribed_users.return_value = [system_user_1, system_user_2]
    router_mock.data_source.get_channel_groups.return_value = []
    router_mock.data_source.get_group_users.assert_not_called()
    router_mock.data_source.get_system_user.assert_not_called()
    assert router_mock.get_channel_users(ANY, "c3ccc15b-298f-4dc7-877f-2c8970331caf") == [system_user_1, system_user_2]


@mock.patch.object(Router, "get_channel_users")
def test_process_message_no_channel_users(mock_get_channel_users, appctx, router_mock, message):
    """Test process message for channel without users."""
    mock_get_channel_users.return_value = []
    assert router_mock.process_message(message) is None


def test_process_users_target_users(
    appctx,
    router_mock,
    group_1,
    group_2,
    system_user_1,
    system_user_2,
    system_user_3,
    group_user_1,
    group_user_2,
    group_user_3,
    no_system_user_1,
):
    """Test get_target_users."""
    router_mock.data_source.get_target_users.return_value = [system_user_1, system_user_2]
    router_mock.data_source.get_target_groups.return_value = [group_1["group_id"], group_2["group_id"]]
    router_mock.data_source.get_channel_unsubscribed_users.return_value = [system_user_1["username"]]
    router_mock.data_source.get_group_users.side_effect = [
        [group_user_1],  # ignored because its on unsub
        [no_system_user_1, group_user_3],
    ]

    router_mock.data_source.get_system_user.side_effect = [
        NotFoundDataSourceError(User, id=no_system_user_1["username"]),
        system_user_3,
    ]

    notif_id = "a"
    channel_id = "b"
    assert router_mock.get_target_users(notif_id, channel_id) == [system_user_2, no_system_user_1, system_user_3]
    router_mock.data_source.get_target_users.assert_called_once_with(notif_id)
    router_mock.data_source.get_target_groups.assert_called_once_with(notif_id)
    router_mock.data_source.get_channel_unsubscribed_users.assert_called_once_with(channel_id)
    assert router_mock.data_source.get_group_users.call_count == 2
    assert router_mock.data_source.get_system_user.call_count == 2


@mock.patch.object(Router, "get_target_users")
@mock.patch.object(Router, "get_channel_users")
def test_process_users_intersection(
    mock_get_channel_users,
    mock_get_target_users,
    appctx,
    router_mock,
    message_intersection,
    system_user_1,
    system_user_2,
    no_system_user_1,
    group_user_1,
):
    """Test process users for intersection."""
    mock_get_channel_users.return_value = [system_user_2, group_user_1]
    mock_get_target_users.return_value = [system_user_1, system_user_2, no_system_user_1]

    result = router_mock.process_users(message_intersection)
    assert result == [system_user_2]
    mock_get_channel_users.assert_called_once_with(message_intersection["id"], message_intersection["channel_id"])
    mock_get_target_users.assert_called_once_with(message_intersection["id"], message_intersection["channel_id"])


@mock.patch.object(Router, "get_target_users")
@mock.patch.object(Router, "get_channel_users")
def test_process_users_private(
    mock_get_channel_users, mock_get_target_users, appctx, router_mock, message_private, group_user_1, system_user_2
):
    """Test process users for private."""
    mock_get_target_users.return_value = [system_user_2, group_user_1]

    result = router_mock.process_users(message_private)
    assert result == [system_user_2, group_user_1]
    mock_get_target_users.assert_called_once_with(message_private["id"], message_private["channel_id"])
    mock_get_channel_users.assert_not_called()


@mock.patch.object(Router, "get_target_users")
@mock.patch.object(Router, "get_channel_users")
def test_process_users(
    mock_get_channel_users, mock_get_target_users, appctx, router_mock, message, system_user_1, system_user_2
):
    """Test process users for normal."""
    mock_get_channel_users.return_value = [system_user_1, system_user_2]

    result = router_mock.process_users(message)
    assert result == [system_user_1, system_user_2]
    mock_get_channel_users.assert_called_once_with(message["id"], message["channel_id"])
    mock_get_target_users.assert_not_called()


@mock.patch("notifications_routing.router.apply_default_preferences")
@mock.patch.object(Router, "get_channel_users")
def test_process_message_no_system_users(
    mock_get_channel_users, mock_apply_default_preferences, appctx, router_mock, message, no_system_user_1
):
    """Test process message for non system users."""
    mock_get_channel_users.return_value = [no_system_user_1]
    router_mock.process_message(message)
    mock_apply_default_preferences.assert_called_once_with(ANY, message, "nosystemuser1@cern.ch")


@mock.patch("notifications_routing.router.apply_user_preferences")
@mock.patch("notifications_routing.router.apply_default_preferences")
@mock.patch.object(Router, "get_channel_users")
def test_process_message_system_user_mute_active(
    mock_get_channel_users,
    mock_apply_default_preferences,
    mock_apply_user_preferences,
    appctx,
    router_mock,
    message,
    system_user_1,
):
    """Test porcess message for system user with mute active."""
    mock_get_channel_users.return_value = [system_user_1]
    router_mock.data_source.is_user_mute_active.return_value = True
    router_mock.process_message(message)
    assert not mock_apply_default_preferences.called
    assert not mock_apply_user_preferences.called


@mock.patch("notifications_routing.router.apply_user_preferences")
@mock.patch.object(Router, "get_channel_users")
def test_process_message_system_user_with_preferences(
    mock_get_channel_users,
    mock_apply_user_preferences,
    appctx,
    router_mock,
    message,
    system_user_1,
    preference,
    target_preference,
):
    """Test process message for system user with preferences."""
    mock_get_channel_users.return_value = [system_user_1]
    router_mock.data_source.is_user_mute_active.return_value = False
    router_mock.data_source.get_user_preferences.return_value = {
        "email": "testuser1@cern.ch",
        "preferences": [preference],
    }
    router_mock.data_source.get_delivery_methods_and_targets.return_value = {target_preference}
    router_mock.process_message(message)
    mock_apply_user_preferences.assert_called_once()


@mock.patch("notifications_routing.router.apply_all")
@mock.patch.object(Router, "get_channel_users")
def test_process_message_critical_message(
    mock_get_channel_users, mock_apply_all, appctx, router_mock, message_critical, system_user_1, device
):
    """Test process message for critical priority."""
    mock_get_channel_users.return_value = [system_user_1]
    router_mock.data_source.get_user_devices.return_value = [device]
    router_mock.process_message(message_critical)
    mock_apply_all.assert_called_once_with(ANY, message_critical, "testuser1@cern.ch", [device])


@mock.patch("notifications_routing.router.apply_default_preferences")
@mock.patch.object(Router, "get_channel_users")
def test_process_message_not_found_datasource_error(
    mock_get_channel_users, mock_apply_default_preferences, appctx, router_mock, message, system_user_1
):
    """Test process message when resource is not found on db."""
    mock_get_channel_users.return_value = [system_user_1]
    router_mock.data_source.is_user_mute_active.return_value = False
    router_mock.data_source.get_user_preferences.side_effect = NotFoundDataSourceError(
        User, id="3eacfc35-42bd-49e8-9f2c-1e552c447ff3"
    )
    router_mock.process_message(message)
    mock_apply_default_preferences.assert_called_once_with(ANY, message, "testuser1@cern.ch")
