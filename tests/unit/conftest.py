"""Package's fixtures."""
import datetime
from unittest.mock import MagicMock

import pytest

from notifications_routing.app import configure_logging
from notifications_routing.config import load_config
from notifications_routing.data_source.postgres.postgres_data_source import Device, Preference
from notifications_routing.preferences import TargetPreference
from notifications_routing.router import Router


@pytest.fixture(scope="module")
def config():
    """Set up config."""
    config = load_config()
    yield config


@pytest.fixture(scope="module")
def appctx(config):
    """Set up app context."""
    configure_logging(config)


@pytest.fixture(scope="function")
def router_mock():
    """Private access record."""
    publisher = MagicMock()

    data_source = MagicMock()
    data_source.USERNAME = "username"
    data_source.EMAIL = "email"
    data_source.LAST_LOGIN = "last_login"
    data_source.USER_ID = "user_id"

    return Router(publisher, data_source)


@pytest.fixture(scope="function")
def system_user_1():
    """User dict."""
    return {
        "user_id": "3eacfc35-42bd-49e8-9f2c-1e552c447ff3",
        "username": "testuser1",
        "email": "testuser1@cern.ch",
        "last_login": datetime.datetime(2012, 2, 1, 1, 0),
    }


@pytest.fixture(scope="function")
def system_user_2():
    """User dict."""
    return {
        "user_id": "356cfcff-42bd-49e8-9f2c-1e552c447ff3",
        "username": "testuser2",
        "email": "testuser2@cern.ch",
        "last_login": datetime.datetime(2012, 2, 1, 1, 0),
    }


@pytest.fixture(scope="function")
def system_user_3():
    """User dict."""
    return {
        "user_id": "444cfcff-42bd-49e8-9f2c-1e552c447ff3",
        "username": "testuser3",
        "email": "testuser3@cern.ch",
        "last_login": datetime.datetime(2012, 2, 1, 1, 0),
    }


@pytest.fixture(scope="function")
def no_system_user_1():
    """No system user dict."""
    return {"username": "nosystemuser1", "email": "nosystemuser1@cern.ch"}


@pytest.fixture(scope="function")
def no_system_user_2():
    """No system user dict."""
    return {"username": "nosystemuser2", "email": "nosystemuser2@cern.ch"}


@pytest.fixture(scope="function")
def no_system_user_unsubscribed():
    """No system user dict."""
    return {"username": "unsubscribeduser", "email": "unsubscribeduser@cern.ch"}


@pytest.fixture(scope="function")
def group_1():
    """Channel user dict."""
    return {"group_id": "0557969f-acd8-433c-b9c9-59ea642b751b"}


@pytest.fixture(scope="function")
def group_2():
    """Channel user dict."""
    return {"group_id": "378ec6a5-cf3f-459f-bb59-0de749edbba3"}


@pytest.fixture(scope="function")
def group_user_1():
    """Channel user dict."""
    return {"username": "testuser1", "email": "testuser1@cern.ch"}


@pytest.fixture(scope="function")
def group_user_2():
    """Channel user dict."""
    return {"username": "testuser2", "email": "testuser2@cern.ch"}


@pytest.fixture(scope="function")
def group_user_3():
    """Channel user dict."""
    return {"username": "testuser3", "email": "testuser3@cern.ch"}


@pytest.fixture(scope="function")
def message():
    """Message dict."""
    return {
        "id": "d53a7d79-0c5e-4b72-9d6f-8492b7415340",
        "channel_id": "c3ccc15b-298f-4dc7-877f-2c8970331caf",
        "channel_name": "Test channel",
        "channel_slug": "test-channel",
        "channel_short_url": "https://cern.ch/thisisanexample",
        "priority": "NORMAL",
        "created_timestamp": "2021-02-26T13:59:40.754Z",
        "message_body": "test",
        "summary": "test",
        "link": None,
        "img_url": None,
        "intersection": False,
        "private": False,
    }


@pytest.fixture(scope="function")
def message_private():
    """Message dict."""
    return {
        "id": "d53a7d79-0c5e-4b72-9d6f-8492b7415340",
        "channel_id": "c3ccc15b-298f-4dc7-877f-2c8970331caf",
        "channel_name": "Test channel",
        "channel_slug": "test-channel",
        "channel_short_url": "https://cern.ch/thisisanexample",
        "priority": "NORMAL",
        "created_timestamp": "2021-02-26T13:59:40.754Z",
        "message_body": "test",
        "summary": "test",
        "link": None,
        "img_url": None,
        "intersection": False,
        "private": True,
    }


@pytest.fixture(scope="function")
def message_intersection():
    """Message dict."""
    return {
        "id": "d53a7d79-0c5e-4b72-9d6f-8492b7415340",
        "channel_id": "c3ccc15b-298f-4dc7-877f-2c8970331caf",
        "channel_name": "Test channel",
        "channel_slug": "test-channel",
        "channel_short_url": "https://cern.ch/thisisanexample",
        "priority": "NORMAL",
        "created_timestamp": "2021-02-26T13:59:40.754Z",
        "message_body": "test",
        "summary": "test",
        "link": None,
        "img_url": None,
        "private": True,
        "intersection": True,
    }


@pytest.fixture(scope="function")
def message_critical():
    """Message dict."""
    return {
        "id": "d53a7d79-0c5e-4b72-9d6f-8492b7415340",
        "channel_id": "c3ccc15b-298f-4dc7-877f-2c8970331caf",
        "channel_name": "Test channel",
        "channel_slug": "test-channel",
        "channel_short_url": "https://cern.ch/thisisanexample",
        "priority": "CRITICAL",
        "created_timestamp": "2021-02-26T13:59:40.754Z",
        "message_body": "test",
        "summary": "test",
        "link": None,
        "img_url": None,
    }


@pytest.fixture(scope="function")
def preference(device):
    """Preference object."""
    return Preference(
        id="17fd2706-8baf-433b-82eb-8c7fada847da",
        type="DAILY",
        name="Daily preference",
        userId="3eacfc35-42bd-49e8-9f2c-1e552c447ff3",
        targetId="c3ccc15b-298f-4dc7-877f-2c8970331caf",
        notificationPriority="NORMAL",
        rangeStart=None,
        rangeEnd=None,
        scheduledTime=datetime.time(13, 0),
        devices=[device],
    )


@pytest.fixture(scope="function")
def device():
    """Device object."""
    return Device(
        id="d4e15d11-a56e-4568-9e9f-497110426a72",
        name="testuser1@cern.ch",
        userId="3eacfc35-42bd-49e8-9f2c-1e552c447ff3",
        info="Default",
        type="MAIL",
        subType=None,
        token=None,
    )


@pytest.fixture(scope="function")
def target_preference(preference):
    """Target Preference object."""
    return TargetPreference(
        preference.type, preference.devices, preference.scheduledTime, preference.scheduledDay, preference.id
    )
