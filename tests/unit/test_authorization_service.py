"""Unit Tests for the Auth service."""
import json
import logging
from unittest import mock
from unittest.mock import call

import pytest
import requests

from notifications_routing.authorization_service import get_group_users_api


class MockResponse:
    """Mock class for requests responses."""

    def __init__(self, json_data, status_code):
        """Initialize the class."""
        self.json_data = json_data
        self.status_code = status_code

    @property
    def content(self):
        """Mock content property."""
        return json.dumps(self.json_data)


@pytest.fixture(scope="function")
def auth_user():
    """Auth user dict."""
    return {
        "primaryAccountEmail": "user1@cern.ch",
        "upn": "user1",
        "unconfirmed": False,
        "unconfirmedEmail": None,
        "externalEmail": None,
        "type": "Person",
        "activeUser": True,
        "source": "cern",
    }


@pytest.fixture(scope="function")
def auth_user_external():
    """Auth user dict."""
    return {
        "primaryAccountEmail": "external@usc.es",
        "upn": "external@1234567890",
        "unconfirmed": False,
        "unconfirmedEmail": None,
        "externalEmail": None,
        "type": "Person",
        # external user don't have activeUser
        # "activeUser": None
        "source": "edugain",
    }


@pytest.fixture(scope="function")
def auth_user_external_unconfirmed():
    """Auth user dict."""
    return {
        "externalEmail": None,
        "primaryAccountEmail": None,
        "type": "Person",
        "upn": None,
        # unconfirmed, external users have source null
        "source": None,
        "unconfirmed": True,
        "unconfirmedEmail": "external-unconfirmed@hotmail.fr",
        # external user don't have activeUser
        # "activeUser": None
    }


@pytest.fixture(scope="function")
def auth_user_data_inconsistent_error():
    """Auth user dict."""
    return {
        "primaryAccountEmail": None,
        "upn": "missing email",
        "unconfirmed": False,
        "unconfirmedEmail": None,
        "type": "Person",
        "externalEmail": None,
        "activeUser": True,
        "source": None,
    }


@pytest.fixture(scope="function")
def auth_group_user_application():
    """Auth user dict."""
    return {
        "primaryAccountEmail": None,
        "upn": "application-test",
        "unconfirmed": False,
        "unconfirmedEmail": None,
        "type": "Application",
        "externalEmail": None,
        "source": None
        # Application type don't have activeUser field
        # "activeUser": None
    }


@pytest.fixture(scope="function")
def auth_group_user_service():
    """Auth user dict."""
    return {
        "primaryAccountEmail": "service-email@cern.ch",
        "upn": "service",
        "unconfirmed": False,
        "unconfirmedEmail": None,
        "type": "Service",
        "externalEmail": None,
        "source": "cern",
        # Service type don't have activeUser field
        # "activeUser": None
    }


@pytest.fixture(scope="function")
def auth_group_user_inactive_1():
    """Auth user dict."""
    return {
        "externalEmail": "external@aol.com",
        "primaryAccountEmail": "inactive1@cern.ch",
        "type": "Person",
        "upn": "inactive1",
        "unconfirmed": False,
        "unconfirmedEmail": None,
        "activeUser": False,
        "source": "cern",
    }


@pytest.fixture(scope="function")
def auth_group_user_inactive_2():
    """Auth user dict."""
    return {
        "externalEmail": None,
        "primaryAccountEmail": "inactive2@cern.ch",
        "type": "Person",
        "upn": "inactive2",
        "unconfirmed": False,
        "unconfirmedEmail": None,
        "activeUser": False,
        "source": "cern",
    }


@pytest.fixture(scope="function")
def auth_group_user_secondary():
    """Auth user dict."""
    return {
        "externalEmail": None,
        "primaryAccountEmail": "secondary@cern.ch",
        "type": "Secondary",
        "upn": "secondary",
        "unconfirmed": False,
        "unconfirmedEmail": None,
        "source": "cern",
        # secondary type don't have activeUser field
        # "activeUser": None
    }


@mock.patch("notifications_routing.authorization_service.get_auth_access_token")
@mock.patch.object(requests, "get")
@mock.patch.object(logging, "error")
def test_process_users(
    mock_logging_error,
    mock_get,
    mock_get_token,
    appctx,
    auth_user,
    auth_user_external,
    auth_user_external_unconfirmed,
    auth_group_user_inactive_1,
    auth_group_user_inactive_2,
    auth_group_user_service,
    auth_group_user_application,
    auth_group_user_secondary,
    auth_user_data_inconsistent_error,
):
    """Test process users for normal."""
    mock_get_token.return_value = "jwt"
    mock_get.side_effect = [
        MockResponse(
            {
                "pagination": {"next": "/next/url/page"},
                "data": [
                    auth_user,
                    auth_user_external,
                    auth_user_external_unconfirmed,
                    auth_group_user_inactive_1,
                    auth_group_user_inactive_2,  # should be ignored, email is not extractable
                    auth_group_user_service,
                    auth_group_user_application,  # should be ignore
                    auth_user_data_inconsistent_error,  # log error
                ],
            },
            200,
        ),
        MockResponse({"pagination": {"next": None}, "data": [auth_group_user_secondary]}, 200),
    ]

    result = get_group_users_api("group_id")
    assert result == [
        {"username": "user1", "email": "user1@cern.ch"},
        {"username": "external@1234567890", "email": "external@usc.es"},
        {"username": "external-unconfirmed@hotmail.fr", "email": "external-unconfirmed@hotmail.fr"},
        {"username": "inactive1", "email": "external@aol.com"},
        {"username": "service", "email": "service-email@cern.ch"},
        {"username": "secondary", "email": "secondary@cern.ch"},
    ]

    mock_logging_error.assert_called_once()
    mock_get_token.assert_called_once()
    expected_first_call_url = (
        "https://authorization-service-api.web.cern.ch"
        "/api/v1.0/Group/group_id/memberidentities/precomputed?field=upn&field=primaryAccountEmail"
        "&field=unconfirmed&field=unconfirmedEmail&field=type&field=externalEmail&field=activeUser&field=source"
    )
    expected_headers = {"Authorization": "Bearer jwt"}
    mock_get.assert_has_calls(
        [
            call(expected_first_call_url, headers=expected_headers),
            call("https://authorization-service-api.web.cern.ch/next/url/page", headers=expected_headers),
        ]
    )
