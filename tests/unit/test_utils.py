"""Unit Tests for Utils."""
import unittest

import notifications_routing.utils as utils


class TestUtils(unittest.TestCase):
    """TestCase Class for Utils Unit Tests."""

    def test_convert_timestamp_to_local_timezone(self):
        """Test that timestamp is converted from utc to local timezone."""
        self.assertEqual(
            utils.convert_timestamp_to_local_timezone("2021-06-09T11:45:19.220445Z"), "06/09/2021, 13:45:19"
        )


if __name__ == "__main__":
    unittest.main()
