# Notifications Routing

## Contribute

* Locally clone the repository

```bash
$ git clone ssh://git@gitlab.cern.ch:7999/push-notifications/notifications-routing.git
$ cd notifications-routing
```

* Make sure you have the latest version

```bash
$ git checkout master
$ git pull
```

* Install [pre-commit](https://pre-commit.com/#install) hooks

## Develop with docker-compose

This is the recommended method for Linux distributions.

### Dependencies
- [Docker](https://docs.docker.com/engine/install/)
- [Docker-compose](https://docs.docker.com/compose/install/)

### Instructions

- Start the routing container, Postgres and ActiveMQ
```bash
$ make docker-build-env
```

There is a `subscriber` client that listens to messages and a helper utility called `publisher` that sends messages.

For the `subscriber`, it will run automatically as it is the container's entrypoint.

For the `publisher` to send one message, it's necessary to open another terminal and enter the container:

- Enter the routing container
```bash
$ make docker-shell-env  # To enter the container
```
A message send can be triggered using the `docker-send.py` script.
```
$ python scripts/docker-send.py # To send one message
```
The payload can also be customized with the following arguments:
```
usage: docker-send.py [-h] [-l] [-c] [-e E] [-ci CI] [-ni NI]
-l          use to connect to localhost instead of activemq
-c          use to send an email with Category
-e E        use to set a target email username
-ci CI      use to set a target channel id
-ni NI      use to set a target notification id
```

To test the set up, it is possible to see the message in [ActiveMQ's admin UI](http://localhost:8161/admin/queues.jsp)

- Clean up the containers
```bash
$ make docker-stop
```


### Known issues (and solutions) for some Linux distributions

- If you have some service (probably postgres) running locally in your machine at port 5432 you have 2 options:
  - Stop your local service
  - Change the external port to a free port (i.e. 5433) at ```docker-compose.full.yml```
    ```
      pg_db:
      ...
      ports:
        - 5433:5432
        ...
    ```
- If the container is not able to connect/find the service:
  - [Disable SELinux](https://www.cyberciti.biz/faq/disable-selinux-on-centos-7-rhel-7-fedora-linux/)
  - [Whitelist docker in firewall](https://fedoramagazine.org/docker-and-fedora-32/) or using Firewall tool from Software store
      ```bash
      $ sudo firewall-cmd --permanent --zone=trusted --add-interface=docker0
      $ sudo firewall-cmd --permanent --zone=FedoraWorkstation --add-masquerade
      ```

### Generic Guidelines

* Acessing ActiveMQ Admin UI:
For accessing the local instance of `ActiveMQ`:  http://localhost:8161/admin

```
User: admin
Password: admin
```

* Develop, test and finish the new feature with regular commits

```bash
$ git add file1 file2
$ git commit
...
$ git push
```

* When ready, rebase interactively against the latest `master` to tidy things up and then create a merge request

```bash
$ git checkout master
$ git pull
$ git checkout dev_short_feature_description
$ git rebase -i master
$ git push
```

[Create a merge request](https://gitlab.cern.ch/push-notifications/notifications-routing/-/merge_requests) from the `dev` branch into `master`. Assign a reviewer. Following a discussion and approval from the reviewer, the merge request can be merged.


### Manage dependencies
We're using [poetry](https://python-poetry.org/docs/cli/) to manage dependencies

:warning:
Only update dependencies inside the docker container, ie. after running `make docker-shell-env`.

- Regenerate lock after manually changing `pyproject.toml`:
```bash
poetry lock
```

- Add a dependency with:
```bash
poetry add "pendulum~2.0.5"
```
See more on choosing [dependency constrains](https://python-poetry.org/docs/versions/).

- Update a dependency:
```bash
poetry update requests
```

### Creating and Running Unit and Integration Tests

#### Writing tests

The folder ```tests```  contains all test cases and future tests should be placed into this folder.
Each test file needs to start with the prefix ```test_``` (e.g. ```test_utils.py```)


Tests functions always start with the prefix ```test_``` e.g.:
```python

def test_FUNCTION_OPERATION(self):
    ...

```

#### Execute Pytest Tests

To manually execute the tests use the following command:

```bash
make ci-test
```
