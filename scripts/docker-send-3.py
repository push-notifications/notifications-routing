"""Utility to send 3 messages inside the docker environment."""
import stomp


conn = stomp.Connection([("activemq", 61613)])
conn.connect("admin", "admin", wait=True)
message_body = r"""{"id":1,"target":{"primaryKey":2,"id":"test-roncero","name":"test roncero",
"description":"test roncero","visibility":"RESTRICTED","subscriptionPolicy":"DYNAMIC","APIKey":null,
"creationDate":"2020-09-07T10:43:52.885Z","lastActivityDate":"2020-09-07T10:43:52.885Z","deleteDate":null},
"body":"<p>test</p>\n","sendAt":null,"sentAt":"2020-09-07T14:53:15.389Z","users":[],"summary":"test","tags":null,
"link":null,"contentType":null,"imgUrl":null,"priority":"NORMAL"} """
conn.send(body=message_body, destination="/queue/np.test", headers={"persistent": "true"})

message_body = r"""{"id":2,"target":{"primaryKey":2,"id":"test-roncero","name":"test roncero",
"description":"test roncero","visibility":"RESTRICTED","subscriptionPolicy":"DYNAMIC","APIKey":null,
"creationDate":"2020-09-07T10:43:52.885Z","lastActivityDate":"2020-09-07T10:43:52.885Z","deleteDate":null},
"body":"<p>test</p>\n","sendAt":null,"sentAt":"2020-09-07T14:53:15.389Z","users":[],"summary":"test","tags":null,
"link":null,"contentType":null,"imgUrl":null,"priority":"NORMAL"} """
conn.send(body=message_body, destination="/queue/np.test", headers={"persistent": "true"})

message_body = r"""{"id":3,"target":{"primaryKey":2,"id":"test-roncero","name":"test roncero",
"description":"test roncero","visibility":"RESTRICTED","subscriptionPolicy":"DYNAMIC","APIKey":null,
"creationDate":"2020-09-07T10:43:52.885Z","lastActivityDate":"2020-09-07T10:43:52.885Z","deleteDate":null},
"body":"<p>test</p>\n","sendAt":null,"sentAt":"2020-09-07T14:53:15.389Z","users":[],"summary":"test","tags":null,
"link":null,"contentType":null,"imgUrl":null,"priority":"NORMAL"} """
conn.send(body=message_body, destination="/queue/np.routing", headers={"persistent": "true"})

conn.disconnect()
