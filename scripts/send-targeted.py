"""Utility to send one targeted message."""
import stomp


conn = stomp.Connection([("localhost", 61613)])
conn.connect("admin", "admin", wait=True)
# noqa
message_body = r"""{"id":"bd19eea4-9dca-48d9-a577-5de9d2bf374a",
"target":{"id":"50b1faf5-ae0d-46a6-8c62-7072fe5d466b","slug":"test","name":"Test","description":"",
"visibility":"RESTRICTED","subscriptionPolicy":"DYNAMIC","archive":false,"APIKey":null,
"creationDate":"2021-02-26T12:58:42.131Z","lastActivityDate":"2021-02-26T12:58:42.131Z","incomingEmail":null,
"deleteDate":null}, "body":"<p>test</p>\n", "sendAt":null,"sentAt":"2021-02-26T13:59:40.754Z","users":[],
"link":"https://test-notifications.web.cern.ch/main/channels/a4db5752-8f58-4144-a4f7-a2b6454273f5/notifications/de213d1c-4438-4f24-9bc3-58c0373db527",
"summary":"test","imgUrl":null,"priority":"LOW","tags":null,"contentType":null,
"private":true,
"targetUsers":["79e92fe3-97a3-4970-a8dd-181caa7b5a10"],
"targetGroups":[]} """

conn.send(body=message_body, destination="/queue/np.routing", headers={"persistent": "true"})
conn.disconnect()
