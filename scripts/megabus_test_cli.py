"""CLI Utility to test megabus flow."""
import argparse
import asyncio
import json
import time

import megabus
import stomp


class ListenerTest(megabus.Listener):
    """Test Listener class."""

    def __init__(self, args):
        """Initialize the listener."""
        self.ack = args.ack
        self.sleeptime = args.sleeptime
        self.skipstep = args.skipstep
        self.skipstep_behavior = args.skipstep_behavior
        self.connections = []
        self.messages = []

    def set_connections(self, connections):
        """Set stomp connections obtained from megabus."""
        self.connections = connections

    def ack_msg(self, message_id):
        """Acknowledge a message."""
        for connection in self.connections:  # type: stomp.Connection
            if connection.is_connected():
                connection.ack(message_id, 1)

    def nack_msg(self, message_id):
        """Send a message back to the queue."""
        for connection in self.connections:
            if connection.is_connected():
                connection.nack(message_id, 1)

    def on_message(self, message, headers):
        """Process a message."""
        message_json = json.loads(message)
        message_id = headers["message-id"]
        print(f"New message {message_id} - {message_json['id']}")
        print(f"Headers {headers}")

        time.sleep(5)

        if 0 < self.skipstep == message_json["id"]:
            if self.skipstep_behavior == "crash":
                raise Exception("An expected Exception has occurred")
            if self.skipstep_behavior == "return":
                return True
            if self.skipstep_behavior == "nack":
                self.nack_msg(message_id)
                return

        if self.sleeptime > 0:
            time.sleep(self.sleeptime)

        if self.ack != "auto":
            self.ack_msg(message_id)

        print("End processing")


async def connect(args):
    """Init the consumer and connect to activeMQ."""
    listener = ListenerTest(args)
    cons = megabus.Consumer("routing_consumer", ack=args.ack, extension="test", auto_connect=False, listener=listener)
    listener.set_connections(cons._connections)
    cons.connect()


if __name__ == "__main__":

    def str2bool(v):
        """Convert str to bool."""
        if isinstance(v, bool):
            return v
        if v.lower() in ("yes", "true", "t", "y", "1"):
            return True
        elif v.lower() in ("no", "false", "f", "n", "0"):
            return False
        else:
            raise argparse.ArgumentTypeError("Boolean value expected.")

    args_parser = argparse.ArgumentParser(
        description="Used for testing the megabus module interaction with ActiveMQ instances"
    )

    # Add the arguments
    args_parser.add_argument(
        "--ack",
        default="auto",
        choices=["auto", "client", "client-individual"],
        help="Determines the way the client sends the ACK message via STOMP Protocl (default:auto)",
    )

    args_parser.add_argument(
        "--sleeptime", default=0, type=int, help="Time the agent sleeps when receiving message (default: 0)"
    )

    args_parser.add_argument(
        "--skipstep", type=int, default=0, help="Determines the id of message to skip processing (default: 0)"
    )

    args_parser.add_argument(
        "--skipstep_behavior",
        default="return",
        choices=["return", "crash", "nack"],
        help="Determines the n-th message to skip processing (default: return)",
    )

    # Execute the parse_args() method
    args = args_parser.parse_args()

    loop = asyncio.get_event_loop()
    loop.run_until_complete(connect(args))
    loop.run_forever()
