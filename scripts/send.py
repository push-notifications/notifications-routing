"""Utility to send one message."""
import stomp


conn = stomp.Connection([("localhost", 61613)])
conn.connect("admin", "admin", wait=True)
# noqa
message_body = r"""{"id":1,"target":{"primaryKey":1,"id":"The Best Notifications",
"name":"The Best Notifications",
"description":"the very best","visibility":"RESTRICTED","subscriptionPolicy":"DYNAMIC","APIKey":null,
"creationDate":"2020-09-07T10:43:52.885Z",
"lastActivityDate":"2020-09-07T10:43:52.885Z",
"deleteDate":null},
"body":"<p>test EO body</p>\n",
"sendAt":null,
"sentAt":"2020-09-07T14:53:15.389Z",
"users":[],"summary":"sub test EO","tags":null,
"link":"http://cds.cern.ch/record/2687667",
"contentType":null,
"private":false,
"imgUrl":"http://cds.cern.ch/record/2687667/files/CLICtd.png?subformat=icon-640",
"priority":"NORMAL"} """
conn.send(body=message_body, destination="/queue/np.routing", headers={"persistent": "true"})
conn.disconnect()
