"""Utility to send one message inside the docker environment."""
import argparse
import json

import stomp


parser = argparse.ArgumentParser(description="Send a test email notification.")
parser.add_argument("-l", action="store_true", default=False, help="use to connect to localhost instead of activemq")
parser.add_argument("-c", action="store_true", default=False, help="use to send an email with Category")
parser.add_argument("-e", default="user", help="use to set a target email username")
parser.add_argument("-ci", default="bb5f841a-5554-4038-98c7-4eacd19acb61", help="use to set a target channel id")
parser.add_argument("-ni", default="D19EEA4-9DCA-48D9-A577-5DE9D2BF374A", help="use to set a target notification id")

args = parser.parse_args()
category = args.c
channelid = args.ci
notificationid = args.ni
email = args.e
localhost = args.l

connection_target = "localhost" if localhost else "activemq"
conn = stomp.Connection([(connection_target, 61613)])
conn.connect("admin", "admin", wait=True)

message = {
    "id": "D19EEA4-9DCA-48D9-A577-5DE9D2BF374A",
    "target": {
        "id": "bb5f841a-5554-4038-98c7-4eacd19acb61",
        "slug": "test slug",
        "category": None,
    },
    "body": "<p>test</p>\n",
    "sentAt": "2021-08-16T10:01:33.173Z",
    "link": "https://test-notifications.web.cern.ch/",
    "summary": "test summary",
    "imgUrl": None,
    "priority": "important",
    "private": False,
    "targetUsers": False,
    "targetGroups": False,
    "intersection": [],
}

message["email"] = email + "@cern.ch"
if channelid:
    message["target"]["id"] = channelid
if notificationid:
    message["id"] = notificationid
if category:
    message["target"]["category_name"] = "Really Cool!!"

print("sending this: \n")
print(json.dumps(message, indent=4))

conn.send(body=json.dumps(message, indent=4), destination="/queue/np.routing", headers={"persistent": "true"})
conn.disconnect()
