# Usage:
# make setup-env         # sets up the environment
# make lint 	         # runs linting tools

setup-env:
	cd src	&& \
	pipenv install --dev --system --deploy --ignore-pipfile
.PHONY: setup-env

lint:
	flake8
.PHONY: lint

pytest:
	docker-compose -f docker-compose.test.yml exec -T notifications-routing /bin/bash -c "pytest tests -vv;"
.PHONY: pytest

docker-build:
	docker build -t notifications-routing . --no-cache --build-arg build_env=development
.PHONY: docker-build

docker-build-test:
	docker-compose -f  docker-compose.test.yml up -d --remove-orphans
.PHONY: docker-build-test

docker-lint:
	docker run --volume=$$(pwd)/:/app notifications-routing make lint
.PHONY: docker-lint

ci-test: docker-build-test pytest
.PHONY: ci-test

test: stop-test docker-build-test pytest
.PHONY: test

build-env:
	docker-compose -f docker-compose.full.yml up --remove-orphans
.PHONY: build-env

rebuild-env:
	docker-compose -f docker-compose.full.yml build --no-cache --parallel
.PHONY: rebuild-env

build-env-local:
	docker-compose -f docker-compose.local.yml build
	docker-compose -f docker-compose.local.yml up --remove-orphans
.PHONY: build-env-local

shell-env:
	docker-compose -f docker-compose.full.yml exec notifications-routing /bin/bash
.PHONY: shell-env

stop:
	docker-compose -f docker-compose.full.yml down --volumes
	docker-compose -f docker-compose.full.yml rm -f
.PHONY: stop

stop-test:
	docker-compose -f docker-compose.test.yml down --volumes
.PHONY: stop-test

job:
	docker-compose -f docker-compose.job.yml up --remove-orphans
.PHONY: job

shell-job:
	docker-compose -f docker-compose.job.yml run notifications-job-daily-feed /bin/bash
.PHONY: shell-job

logs:
	docker-compose -f docker-compose.full.yml logs -f
.PHONY: logs
