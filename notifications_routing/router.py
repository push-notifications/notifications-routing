"""Notifications Router definition."""
import json
import logging
from typing import Dict, List

import megabus

from notifications_routing.auditing import audit_notification
from notifications_routing.config import Config
from notifications_routing.data_source.data_source import DataSource
from notifications_routing.exceptions import NotFoundDataSourceError
from notifications_routing.preferences import (
    apply_all,
    apply_default_preferences,
    apply_user_preferences,
    get_delivery_methods_and_targets,
)
from notifications_routing.utils import (
    AuditingEvents,
    InputMessageKeys,
    OutputMessageKeys,
    convert_timestamp_to_local_timezone,
)


# ActiveMQ
HEADERS_MESSAGE_ID = "message-id"


class Router(megabus.Listener):
    """Router consumer class."""

    def __init__(self, publisher: megabus.Publisher, data_source: DataSource):
        """Initialize the Router consumer."""
        self.publisher = publisher
        self.data_source = data_source

    def on_message(self, message, headers):
        """Process a message."""
        logging.info("Received message - %s", headers[HEADERS_MESSAGE_ID])
        logging.debug("Message Data: message:%s, headers:%s", message, headers)

        try:
            self.process_message(self.read_message(message))
            self.ack_message(headers[HEADERS_MESSAGE_ID])
        except Exception:
            self.nack_message(headers[HEADERS_MESSAGE_ID])
            logging.exception("An exception happened while processing the message")

    @staticmethod
    def read_message(message):
        """Convert message to json and retrieves target, body, created timestamp, priority."""
        message_json = json.loads(message)

        channel = message_json[str(InputMessageKeys.TARGET)]

        category_name = (
            channel[str(InputMessageKeys.CATEGORY)][str(InputMessageKeys.NAME)]
            if channel.get(str(InputMessageKeys.CATEGORY))
            else None
        )

        notification = {
            str(OutputMessageKeys.ID): message_json[str(InputMessageKeys.ID)],
            str(OutputMessageKeys.CHANNEL_ID): channel[str(InputMessageKeys.ID)],
            str(OutputMessageKeys.CHANNEL_NAME): channel[str(InputMessageKeys.NAME)],
            str(OutputMessageKeys.CHANNEL_SLUG): channel[str(InputMessageKeys.SLUG)],
            str(OutputMessageKeys.CHANNEL_SHORT_URL): channel.get(str(InputMessageKeys.SHORT_URL)),
            str(OutputMessageKeys.PRIORITY): message_json[str(InputMessageKeys.PRIORITY)],
            str(OutputMessageKeys.CREATED_TIMESTAMP): convert_timestamp_to_local_timezone(
                message_json[str(InputMessageKeys.SENT_AT)]
            ),
            str(OutputMessageKeys.MESSAGE_BODY): message_json[str(InputMessageKeys.BODY)],
            str(OutputMessageKeys.SUMMARY): message_json[str(InputMessageKeys.SUMMARY)],
            str(OutputMessageKeys.LINK): message_json[str(InputMessageKeys.LINK)],
            str(OutputMessageKeys.IMGURL): message_json[str(InputMessageKeys.IMGURL)],
            str(OutputMessageKeys.CHANNEL_CATEGORY): category_name,
            str(OutputMessageKeys.PRIVATE): message_json[str(InputMessageKeys.PRIVATE)],
            str(OutputMessageKeys.INTERSECTION): message_json[str(InputMessageKeys.INTERSECTION)],
        }

        return notification

    def add_users_from_groups(
        self,
        notification_id: str,
        channel_id: str,
        users: List[Dict[str, str]],
        groups: List[str],
        unsubscribed_users: List[str],
    ):
        """Add users from groups to users list."""
        unique_usernames = [channel_user[self.data_source.USERNAME] for channel_user in users]
        logging.debug("notification %s channel usernames: %s", notification_id, unique_usernames)

        for group_id in groups:
            group_users = self.data_source.get_group_users(group_id)
            audit_notification(
                notification_id, {"event": AuditingEvents.GET_GROUP_USERS, "group": group_id, "users": group_users}
            )
            logging.debug("channel %s groups users %s", channel_id, group_users)

            for user in group_users:
                if user[self.data_source.USERNAME] in unique_usernames:
                    continue
                # Skip unsubscribed users
                if user[self.data_source.USERNAME] in unsubscribed_users:
                    continue

                try:
                    system_user = self.data_source.get_system_user(user[self.data_source.USERNAME])
                    users.append(system_user)
                    unique_usernames.append(user[self.data_source.USERNAME])
                except NotFoundDataSourceError:
                    users.append(user)
                    unique_usernames.append(user[self.data_source.USERNAME])

        logging.debug("notification %s final users %s", notification_id, users)

    def get_channel_users(self, notification_id, channel_id):
        """Join users from our data source and the grappa system to return a unique users list."""
        users = self.data_source.get_channel_subscribed_users(channel_id)
        groups = self.data_source.get_channel_groups(channel_id)
        logging.debug("channel %s groups %s", users, groups)

        unsubscribed_users = self.data_source.get_channel_unsubscribed_users(channel_id)
        logging.debug("channel %s unsubscribed users %s", channel_id, unsubscribed_users)
        audit_notification(
            notification_id, {"event": AuditingEvents.UNSUSCRIBED_USERS, "targets": unsubscribed_users}, external=True
        )
        audit_notification(
            notification_id,
            {
                "event": AuditingEvents.CHANNEL_SNAPSHOT,
                "target users": users + unsubscribed_users,
                "target groups": groups,
            },
            external=True,
        )

        if groups:
            self.add_users_from_groups(notification_id, channel_id, users, groups, unsubscribed_users)

        logging.debug("channel %s final users %s", channel_id, users)

        return users

    def get_target_users(self, notification_id, channel_id):
        """Join users from our data source and the grappa system to return a unique users list."""
        target_users = self.data_source.get_target_users(notification_id)
        logging.debug("channel %s targeted users %s", channel_id, target_users)

        target_groups = self.data_source.get_target_groups(notification_id)
        logging.debug("channel %s targeted groups %s", channel_id, target_groups)

        audit_notification(
            notification_id,
            {
                "event": AuditingEvents.NOTIFICATION_TARGETS,
                "targets users": target_users,
                "target_groups": target_groups,
            },
            external=True,
        )
        if not (target_groups or target_users):
            return []

        unsubscribed_users = self.data_source.get_channel_unsubscribed_users(channel_id)
        logging.debug("channel %s unsubscribed users %s", channel_id, unsubscribed_users)
        audit_notification(
            notification_id, {"event": AuditingEvents.UNSUSCRIBED_USERS, "targets": unsubscribed_users}, external=True
        )

        subscribed_target_users = [user for user in target_users if user[DataSource.USERNAME] not in unsubscribed_users]
        logging.debug("channel %s subscribed targeted users %s", channel_id, target_users)

        if target_groups:
            self.add_users_from_groups(
                notification_id, channel_id, subscribed_target_users, target_groups, unsubscribed_users
            )

        return subscribed_target_users

    def process_users(self, message) -> List[Dict[str, str]]:
        """Process unique users list, if targeted, intersection or standard post.

        :param message: Dict with keys from OutputMessageKeys
        :return: A list of user data in the format of dicts containing user_id, username, email and last_login
        :raises: NotFoundDataSourceError, MultipleResultsFoundError
        """
        if OutputMessageKeys.PRIVATE in message and message[OutputMessageKeys.PRIVATE]:
            audit_notification(
                message[OutputMessageKeys.ID],
                {"event": AuditingEvents.START_PROCESSING_TARGETED_NOTIFICATION},
                external=True,
            )
            logging.debug("Processing direct notification %s", message[OutputMessageKeys.ID])
            target_users = self.get_target_users(
                message[OutputMessageKeys.ID],
                message[OutputMessageKeys.CHANNEL_ID],
            )

            if not target_users:
                logging.debug("no target_users found for notification %s", message[OutputMessageKeys.ID])
                return []
            if OutputMessageKeys.INTERSECTION in message and message[OutputMessageKeys.INTERSECTION]:
                logging.debug("Processing intersection target users for %s", message[OutputMessageKeys.ID])
                channel_users = self.get_channel_users(
                    message[OutputMessageKeys.ID], message[OutputMessageKeys.CHANNEL_ID]
                )

                audit_notification(
                    message[OutputMessageKeys.ID],
                    {"event": AuditingEvents.TARGET_USERS, "total": len(target_users), "users": target_users},
                    external=True,
                )
                audit_notification(
                    message[OutputMessageKeys.ID],
                    {
                        "event": AuditingEvents.CHANNEL_USERS_FOR_INTERSECTION,
                        "total": len(channel_users),
                        "users": channel_users,
                    },
                    external=True,
                )
                if not channel_users:
                    logging.debug("no channel_users to intersect for channel %s", message[OutputMessageKeys.CHANNEL_ID])
                    return []
                target_users = [x for x in target_users if x in channel_users]

            return target_users
        else:
            audit_notification(message[OutputMessageKeys.ID], {"event": AuditingEvents.START_PROCESSING}, external=True)
            logging.debug("Processing general notification %s", message[OutputMessageKeys.ID])
            target_users = self.get_channel_users(message[OutputMessageKeys.ID], message[OutputMessageKeys.CHANNEL_ID])
            if not target_users:
                logging.debug("no channel_users for channel %s", message[OutputMessageKeys.CHANNEL_ID])
                return []

            return target_users

    def process_message(self, message):
        """Process a message according to user and default preferences and sends to available delivery channels."""
        target_users = self.process_users(message)
        audit_notification(
            message[OutputMessageKeys.ID],
            {"event": AuditingEvents.EXPANDED_RECIPIENTS, "total": len(target_users), "targets": target_users},
            external=True,
        )

        if not target_users:
            return

        for user in target_users:
            # Never logged in users, apply default preferences
            has_logged_in = self.data_source.LAST_LOGIN in user and user.get(self.data_source.LAST_LOGIN)
            if self.data_source.USER_ID not in user or not has_logged_in:
                # User not registered in the notifications service (coming from grappa groups)
                apply_default_preferences(self.publisher, message, user[self.data_source.EMAIL])
                continue

            # CRITICAL PRIORITY: bypass MUTE and send to all devices and mail and override preferences
            if message[OutputMessageKeys.PRIORITY].lower() == Config.CRITICAL_PRIORITY.lower():
                user_devices = self.data_source.get_user_devices(user[self.data_source.USER_ID])
                apply_all(self.publisher, message, user[self.data_source.EMAIL], user_devices)
                continue

            # Check if Mute is activated and skip if true
            if self.data_source.is_user_mute_active(
                user[self.data_source.USER_ID], message[OutputMessageKeys.CHANNEL_ID]
            ):
                audit_notification(
                    message[OutputMessageKeys.ID],
                    {"event": "Mute active, skipped user"},
                    user_id=user[self.data_source.EMAIL],
                )
                logging.info(
                    "MUTE enabled for user %s for channel %s, skipping",
                    user[self.data_source.USER_ID],
                    message[OutputMessageKeys.CHANNEL_ID],
                )
                continue

            # Active User, apply user preferences
            try:
                user_preferences = self.data_source.get_user_preferences(
                    user[self.data_source.USER_ID], message[OutputMessageKeys.CHANNEL_ID]
                )
                delivery_methods_and_targets = get_delivery_methods_and_targets(
                    message[OutputMessageKeys.CREATED_TIMESTAMP], message[OutputMessageKeys.PRIORITY], user_preferences
                )

                # sending to devices per delivery method extracted from preferences
                apply_user_preferences(self.publisher, self.data_source, delivery_methods_and_targets, message, user)

                if not delivery_methods_and_targets:
                    audit_notification(
                        message[OutputMessageKeys.ID],
                        {"event": "No preferences, skipped user"},
                        user_id=user[self.data_source.EMAIL],
                    )

            # Resource not found in the DB, try to fallback to sending by email and continue for other users
            # Dont catch all exceptions - could cause the notification to be successfully processed and be lost
            # and not be retried
            except NotFoundDataSourceError:
                logging.exception("Something failed while retrieving or applying preferences:")
                apply_default_preferences(self.publisher, message, user[self.data_source.EMAIL])
