"""Auditing Router definition."""
import copy
import json
import logging
import uuid
from datetime import datetime

from etcd3 import Client
from etcd3.errors import ErrInvalidAuthToken

from notifications_routing.config import Config
from notifications_routing.utils import AuditingEvents


EXTERNAL_AUDIT_ROUTE_PREFIX = "/external"

client = None
if Config.AUDITING:
    client = Client(host=Config.ETCD_HOST, port=Config.ETCD_PORT)
    if Config.ETCD_USER:
        client.auth(username=Config.ETCD_USER, password=Config.ETCD_PASSWORD)


def _put(notification_id, value, user_id, key, prefix=""):
    """Create an audit log."""
    client.put(
        (
            f"{prefix}/notifications/{notification_id}/{Config.AUDIT_ID}"
            f"/{'target_users/' + user_id + '/' if user_id else ''}{key}"
        ),
        json.dumps({"date": datetime.now().strftime("%d/%m/%Y %H:%M:%S"), **value}, default=str),
    )


def _external_values(value):
    """Remove and clean IDs to prepare for external auditing exposure."""
    filtered_value = copy.deepcopy(value)
    if value["event"] in [
        AuditingEvents.GET_GROUP_USERS,
        AuditingEvents.TARGET_USERS,
        AuditingEvents.CHANNEL_USERS_FOR_INTERSECTION,
    ]:
        del filtered_value["users"]
    if value["event"] == AuditingEvents.UNSUSCRIBED_USERS:
        filtered_value.update({"targets": len(value["targets"])})
    if value["event"] == AuditingEvents.EXPANDED_RECIPIENTS:
        del filtered_value["targets"]
    return filtered_value


def audit_notification(notification_id, value, user_id=None, key=None, external=False):
    """Put audit notification information into audit DB."""

    def _audit():
        _put(notification_id, value, user_id, key)
        if external:
            _put(notification_id, _external_values(value), user_id, key, prefix=EXTERNAL_AUDIT_ROUTE_PREFIX)

    if Config.AUDITING is False:
        logging.info("Audit disabled")
        return

    if not key:
        key = uuid.uuid4()
    try:
        _audit()
    except ErrInvalidAuthToken:
        logging.debug("refresh etcd token")
        client.auth(username=Config.ETCD_USER, password=Config.ETCD_PASSWORD)
        _audit()
    except Exception:
        logging.exception("Error auditing to etcd3:")


def get_audit_notification(notification_id, key, user_id=None):
    """Get audit notification information from audit DB."""
    if Config.AUDITING is False:
        logging.info("Audit disabled")
        return

    def get():
        kvs = client.range(
            (
                f"/notifications/{notification_id}/{Config.AUDIT_ID}"
                f"/{'target_users/' + user_id + '/' if user_id else ''}{key}"
            )
        ).kvs
        if kvs:
            return json.loads(kvs[0].value)
        return None

    try:
        return get()
    except ErrInvalidAuthToken:
        logging.debug("refresh etcd token")
        client.auth(username=Config.ETCD_USER, password=Config.ETCD_PASSWORD)
        return get()
    except Exception:
        logging.exception("Error getting from etcd")
        return None
