"""Build message for activeMQ."""
import json
from notifications_routing.data_source.postgres.postgres_data_source import Device
from notifications_routing.utils import OutputMessageKeys


def convert_notifications_sms_to_json_string(message, device: Device, email: str):
    """Convert notification sms to json string."""
    notif = {
        "message": {
            "channel_id": message[OutputMessageKeys.CHANNEL_ID],
            "notification_id": message[OutputMessageKeys.ID],
            "message_body": message[OutputMessageKeys.MESSAGE_BODY],
            "short_url": message[OutputMessageKeys.CHANNEL_SHORT_URL],
            "summary": message[OutputMessageKeys.SUMMARY],
        },
        "user": {
            "email": email,
        },
        "device": convert_device_to_json(device),
    }
    return json.dumps(notif)


def convert_notification_email_to_json_string(message, device: Device, email: str):
    """Convert notification e-mail to json string."""
    notif = {
        "message": {
            "channel_id": message[OutputMessageKeys.CHANNEL_ID],
            "channel_name": message[OutputMessageKeys.CHANNEL_NAME],
            "message_body": message[OutputMessageKeys.MESSAGE_BODY],
            "summary": message[OutputMessageKeys.SUMMARY],
            "link": message[OutputMessageKeys.LINK],
            "img_url": message[OutputMessageKeys.IMGURL],
            "priority": message[OutputMessageKeys.PRIORITY],
            "notification_id": message[OutputMessageKeys.ID],
            "created_at": message[OutputMessageKeys.CREATED_TIMESTAMP],
            "category_name": message[OutputMessageKeys.CHANNEL_CATEGORY],
            "private": message.get(OutputMessageKeys.PRIVATE) or False,
        },
        "user": {
            "email": email,
        },
        "device": convert_device_to_json(device),
    }

    return json.dumps(notif)


def convert_notification_push_to_json_string(message, device: Device, email: str, encoding=None):
    """Convert notification push to json string.

    encoding: allows to specify aesgcm as encryption instead of default aes128gcm (mostly for test Windows UWP app)
    """
    return json.dumps(
        {
            "message": {
                "channel_id": message[OutputMessageKeys.CHANNEL_ID],
                "channel_name": message[OutputMessageKeys.CHANNEL_NAME],
                "message_body": message[OutputMessageKeys.MESSAGE_BODY],
                "summary": message[OutputMessageKeys.SUMMARY],
                "link": message[OutputMessageKeys.LINK],
                "img_url": message[OutputMessageKeys.IMGURL],
                "encoding": encoding,
                "priority": message[OutputMessageKeys.PRIORITY],
                "notification_id": message[OutputMessageKeys.ID],
                "created_at": message[OutputMessageKeys.CREATED_TIMESTAMP],
                "private": message.get(OutputMessageKeys.PRIVATE) or False,
            },
            "user": {
                "email": email,
            },
            "device": convert_device_to_json(device),
        }
    )


def convert_device_to_json(device: Device):
    """Convert device object to json string."""
    return {
        "id": str(device.id),
        "name": device.name,
        "info": device.info,
        "type": device.type,
        "subType": device.subType,
        "token": device.token,
        "status": device.status,
    }
