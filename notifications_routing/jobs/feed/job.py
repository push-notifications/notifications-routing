"""Feed Job implementation."""
import json
import logging
from datetime import datetime

from notifications_routing.config import Config
from notifications_routing.data_source.data_source import DataSource
from notifications_routing.jobs.job import Job
from notifications_routing.jobs.registry import JobRegistry


@JobRegistry.register
class FeedJob(Job):
    """Prepare daily feed email notifications."""

    __id = "feed"

    def __init__(self, **kwargs):
        """Initialize the job."""
        super().__init__(**kwargs)
        self.publisher = kwargs["publisher"]
        self.data_source = kwargs["data_source"]  # type: DataSource
        self.config = kwargs["config"]

    def __str__(self):
        """Return string representation."""
        return f"Job:{self.id()}:{self.config.SCHEDULED_TIME}"

    @classmethod
    def id(cls):
        """Return the job id."""
        return cls.__id

    def run(self, **kwargs):
        """Prepare feed and cleanup."""
        super().run(**kwargs)

        runtime_datetime = datetime.fromtimestamp(int(Config.SCHEDULED_TIME))
        runtime_time = runtime_datetime.time()
        runtime_day_of_week = runtime_datetime.weekday()

        logging.debug("Delivery Datetime: %s", runtime_datetime)

        for (user_id,) in self.data_source.get_unique_users_from_feed_notifications(
            runtime_datetime, runtime_time, runtime_day_of_week, Config.FREQUENCY_TYPE
        ):
            notifications = self.data_source.get_user_feed_notifications_ids(
                user_id, runtime_datetime, runtime_time, runtime_day_of_week, Config.FREQUENCY_TYPE
            )
            logging.debug("User: %s -  Notifications: %s", user_id, notifications)
            if not notifications:
                continue

            self.send_message(
                message={"user_id": str(user_id), "notifications": notifications}, queue=Config.FEED_QUEUE
            )
            self.data_source.delete_user_feed_notifications(
                user_id, runtime_datetime, runtime_time, runtime_day_of_week, Config.FREQUENCY_TYPE
            )

    def send_message(self, message: dict, queue: str) -> None:
        """Send message to queue.

        :param message: message object
        """
        logging.debug("Sending to queue: %s", message)
        self.publisher.send(json.dumps(message), extension=queue, headers={"persistent": "true"})
