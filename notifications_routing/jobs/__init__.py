"""Import jobs.

Required to auto register the job: force interpreter to load them.
"""

from notifications_routing.jobs.feed.job import FeedJob
