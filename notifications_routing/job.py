"""Notifications Generic Job Runner definition and creation."""
import logging
import logging.config

import yaml

from notifications_routing.config import load_config
from notifications_routing.jobs.registry import JobRegistry


class JobRunner:
    """Notifications Job Runner."""

    def __init__(self, config):
        """Initialize the job runner class."""
        self.config = config

    def run(self):
        """Init all dependencies and run the job."""
        job = JobRegistry.registry[self.config.JOB]

        logging.info("Finished initialising")
        job.run()
        logging.info("Finished running")


def configure_logging(config):
    """Configure logs."""
    with open(config.LOGGING_CONFIG_FILE, "r") as file:
        config = yaml.safe_load(file.read())
        logging.config.dictConfig(config)


def create_job():
    """Create a new JobRunner Class."""
    config = load_config()
    if config.SENTRY_DSN:
        import sentry_sdk

        sentry_sdk.init(dsn=config.SENTRY_DSN)

    configure_logging(config)

    return JobRunner(config)


def run():
    """Run job."""
    app = create_job()
    app.run()
