"""Generic utils."""
from datetime import datetime
from enum import Enum
from dateutil import tz


class StrEnum(str, Enum):
    """Enum where members are also (and must be) strings."""

    def __str__(self):
        """Return string representation."""
        return str(self.value)


class InputMessageKeys(StrEnum):
    """Incoming message keys."""

    TARGET = "target"
    ID = "id"
    NAME = "name"
    CATEGORY = "category"
    SLUG = "slug"
    SHORT_URL = "shortUrl"
    PRIORITY = "priority"
    SENT_AT = "sentAt"
    BODY = "body"
    SUMMARY = "summary"
    LINK = "link"
    IMGURL = "imgUrl"
    PRIVATE = "private"
    INTERSECTION = "intersection"


class OutputMessageKeys(StrEnum):
    """Outgoing message keys."""

    CHANNEL_ID = "channel_id"
    CHANNEL_NAME = "channel_name"
    CHANNEL_CATEGORY = "category_name"
    CHANNEL_SLUG = "channel_slug"
    CHANNEL_SHORT_URL = "channel_short_url"
    PRIORITY = "priority"
    CREATED_TIMESTAMP = "created_timestamp"
    MESSAGE_BODY = "message_body"
    SUMMARY = "summary"
    LINK = "link"
    IMGURL = "img_url"
    ID = "id"
    PRIVATE = "private"
    INTERSECTION = "intersection"
    # PHONE_EMAIL = "phone_email"


class FeedFrequency(StrEnum):
    """Feed frequency types."""

    DAILY = "DAILY"
    WEEKLY = "WEEKLY"
    MONTHLY = "MONTHLY"


class AuditingEvents:
    """Audit event types."""

    GET_GROUP_USERS = "Get group users"
    UNSUSCRIBED_USERS = "Unsubscribed users"
    START_PROCESSING_TARGETED_NOTIFICATION = "Start processing targeted notification"
    START_PROCESSING = "Start processing"
    TARGET_USERS = "Target users"
    CHANNEL_USERS_FOR_INTERSECTION = "Computed users for intersection"
    EXPANDED_RECIPIENTS = "Expanded recipients"
    CHANNEL_SNAPSHOT = "Channel snapshot"
    NOTIFICATION_TARGETS = "Notification targets"


def is_time_between(time, start_range, end_range):
    """Check if time is between a range.

    Examples:
            # Daytime range
            is_time_between(time(9,0), time(17,0), time(10,0))

            # Range crosses midnight
            is_time_between(time(17,0), time(9,0), time(18,0))
            is_time_between(time(17,0), time(9,0), time(8,0))
    """
    range_crosses_midnight = end_range < start_range
    if range_crosses_midnight:
        return time >= start_range or time <= end_range

    return start_range <= time <= end_range


def get_time_from_timestamp(timestamp):
    """Get time from timestamp."""
    return datetime.strptime(timestamp, "%m/%d/%Y, %H:%M:%S").time()


def is_created_time_between_allowed_range(created_timestamp, range_start, range_end):
    """Check if the creation timestamp of the notification is valid.

    Checks if date is inside the time window specified by the user's preferences.
    """
    if not range_start:
        return True

    created_time = get_time_from_timestamp(created_timestamp)

    return is_time_between(created_time, range_start, range_end)


def convert_timestamp_to_local_timezone(created_timestamp):
    """Convert timestamp from utc to local timezone."""
    created_timestamp_at_utc = datetime.strptime(created_timestamp, "%Y-%m-%dT%H:%M:%S.%fZ").replace(tzinfo=tz.tzutc())

    return created_timestamp_at_utc.astimezone(tz.tzlocal()).strftime("%m/%d/%Y, %H:%M:%S")
