"""Consumer entrypoint."""
from notifications_routing import app


if __name__ == "__main__":
    app.run()
