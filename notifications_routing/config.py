"""Configuration definitions."""
import ast
import os


ENV_DEV = "development"
ENV_PROD = "production"


class Config:
    """App configuration."""

    ENV = os.getenv("ENV", ENV_DEV)
    DEBUG = ast.literal_eval(os.getenv("DEBUG", "False"))
    LOGGING_CONFIG_FILE = os.getenv("LOGGING_CONFIG_FILE", "logging.yaml")

    # Sentry
    SENTRY_DSN = os.getenv("SENTRY_DSN")

    # Auth
    CERN_OIDC_CLIENT_ID = os.getenv("CERN_OIDC_CLIENT_ID")
    CERN_OIDC_CLIENT_SECRET = os.getenv("CERN_OIDC_CLIENT_SECRET")
    CERN_ACCESS_TOKEN_URL = os.getenv("CERN_ACCESS_TOKEN_URL", "https://auth.cern.ch/auth/realms/cern/api-access/token")
    CERN_ACCESS_TOKEN_HEADERS = ast.literal_eval(
        os.getenv("CERN_ACCESS_TOKEN_HEADERS", "{'Content-Type': 'application/x-www-form-urlencoded'}")
    )
    CERN_ACCESS_TOKEN_DATA = os.getenv(
        "CERN_ACCESS_TOKEN_DATA",
        f"grant_type=client_credentials&client_id={CERN_OIDC_CLIENT_ID}"
        f"&client_secret={CERN_OIDC_CLIENT_SECRET}&audience=authorization-service-api",
    )
    CERN_AUTH_SERVICE_URL = "https://authorization-service-api.web.cern.ch"
    CERN_GROUP_URL = os.getenv("CERN_GROUP_URL", "/api/v1.0/Group")
    CERN_GROUP_QUERY = os.getenv(
        "CERN_GROUP_QUERY",
        "memberidentities/precomputed?field=upn&field=primaryAccountEmail"
        "&field=unconfirmed&field=unconfirmedEmail&field=type&field=externalEmail&field=activeUser&field=source",
    )

    # DB
    SQLALCHEMY_DATABASE_URI = ("postgresql+psycopg2://{user}:{password}@{host}:{port}/{database}").format(
        user=os.getenv("DB_USER"),
        password=os.getenv("DB_PASSWORD"),
        host=os.getenv("DB_HOST"),
        port=os.getenv("DB_PORT"),
        database=os.getenv("DB_NAME"),
    )
    SQLALCHEMY_TRACK_MODIFICATIONS = ast.literal_eval(os.getenv("SQLALCHEMY_TRACK_MODIFICATIONS", "False"))
    DB_SCHEMA = os.getenv("DB_SCHEMA")
    QUERY_BATCH_SIZE = int(os.getenv("QUERY_BATCH_SIZE", 100))

    # ActiveMQ
    CONSUMER_NAME = os.getenv("CONSUMER_NAME", "routing_consumer")
    PUBLISHER_NAME = os.getenv("PUBLISHER_NAME", "routing_publisher")
    TTL = int(os.getenv("TTL", 172800))

    # Push Notifications backend
    FEED_METHODS = {"DAILY", "WEEKLY", "MONTHLY"}
    CRITICAL_PRIORITY = "CRITICAL"

    # ----- Jobs ----- #
    JOB = os.getenv("JOB", "feed")
    SCHEDULED_TIME = os.getenv("SCHEDULED_TIME")
    FREQUENCY_TYPE = os.getenv("FREQUENCY_TYPE", "DAILY")
    FEED_QUEUE = os.getenv("FEED_QUEUE", "email-daily")

    # Etcd auditing
    ETCD_HOST = os.getenv("ETCD_HOST", "localhost")
    ETCD_PORT = os.getenv("ETCD_PORT", 2379)
    AUDITING = os.getenv("AUDITING", False)
    ETCD_USER = os.getenv("ETCD_USER", None)
    ETCD_PASSWORD = os.getenv("ETCD_PASSWORD", None)
    AUDIT_ID = os.getenv("AUDIT_ID", "router")


class DevelopmentConfig(Config):
    """Development configuration overrides."""

    DEBUG = True


class ProductionConfig(Config):
    """Production configuration overrides."""

    DEBUG = False


def load_config():
    """Load the configuration."""
    config_options = {"development": DevelopmentConfig, "production": ProductionConfig}

    environment = os.getenv("ENV", ENV_DEV)
    return config_options[environment]
