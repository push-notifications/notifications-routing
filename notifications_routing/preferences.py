"""Preferences management."""
import logging
from datetime import datetime
from typing import Dict, List, Set

import megabus
from megabus import Publisher

from notifications_routing.auditing import audit_notification
from notifications_routing.config import Config
from notifications_routing.data_source.data_source import DataSource
from notifications_routing.data_source.postgres.postgres_data_source import Device, Preference, UserFeedNotification
from notifications_routing.exceptions import DuplicatedError
from notifications_routing.utils import (
    OutputMessageKeys,
    is_created_time_between_allowed_range,
)
from notifications_routing.message_builder import (
    convert_notification_email_to_json_string,
    convert_notification_push_to_json_string,
    convert_notifications_sms_to_json_string,
)


class TargetPreference:
    """Target Preference class.

    Holds method and devices together.
    """

    def __init__(self, method, devices, scheduled_time, scheduled_day, preference_id):
        """Initialize the Target Preference."""
        self.method = method
        self.devices = devices
        self.scheduled_time = scheduled_time
        self.scheduled_day = scheduled_day
        self.preference_id = preference_id


def get_delivery_methods_and_targets(
    created_timestamp: str, priority: str, preferences: Dict[str, List[Preference]]
) -> Set[TargetPreference]:
    """Specific implementation of get user's delivery methods.

    :param created_timestamp: Created timestamp of message
    :param priority: Priority of message
    :param preferences: Preferences of user
    """
    delivery_methods_and_targets = set()

    for preference in preferences[DataSource.PREFERENCES]:
        is_in_timewindow = is_created_time_between_allowed_range(
            created_timestamp, preference.rangeStart, preference.rangeEnd
        )
        if not is_in_timewindow:
            logging.debug("Skipping Preference: Not in time window. For: %s", preference.userId)
            continue

        if preference.notificationPriority and priority.lower() not in preference.notificationPriority.lower().split(
            ","
        ):
            logging.debug("Skipping Preference: doesn't match priority. For: %s", preference.userId)
            continue

        logging.debug("Checking User Preferences method: %s", preference.type)
        for device in preference.devices:
            logging.debug("\tUser Preferences devices: %s", device.name)

        delivery_methods_and_targets.add(
            TargetPreference(
                preference.type, preference.devices, preference.scheduledTime, preference.scheduledDay, preference.id
            )
        )

    return delivery_methods_and_targets


def create_feed_notification(
    data_source: DataSource, message: Dict, user: Dict, frequency_type: str, delivery_time: datetime, delivery_day: int
):
    """Create a feed notification for the user and handle duplicated preferences from users.

    :param data_source: Data source
    :param message: message object
    :param user: User data dict
    :param frequency_type: delivery method string
    :param delivery_time: datetime object
    :param delivery_day: delivery day int
    """
    logging.info("Delivery Method %s", frequency_type)
    user_feed_notification = UserFeedNotification(
        user[data_source.USER_ID],
        message[OutputMessageKeys.ID],
        frequency_type,
        message[OutputMessageKeys.CREATED_TIMESTAMP],
        delivery_time,
        delivery_day,
    )

    try:
        data_source.create_feed_user_notification(user_feed_notification)
    except DuplicatedError:
        logging.info("Skipping duplicated daily feed preference for user %s", user[data_source.USER_ID])


def apply_user_preferences(
    publisher: megabus.Publisher,
    data_source: DataSource,
    delivery_methods_and_targets: Set[TargetPreference],
    message: Dict,
    user: Dict,
):
    """Specific implementation of apply user's preferences.

    :param publisher: Publisher object used to publish messages
    :param data_source: Data source
    :param delivery_methods_and_targets: Deliver methods and their targets
    :param message: message object
    :param user: User data dict
    """
    for preference in delivery_methods_and_targets:
        logging.debug("Applying User Preferences delivery method: %s", preference.method)
        if preference.method in Config.FEED_METHODS:
            create_feed_notification(
                data_source, message, user, preference.method, preference.scheduled_time, preference.scheduled_day
            )
            audit_notification(
                message[OutputMessageKeys.ID],
                {
                    "event": "Preference added to feed",
                    "method": preference.method,
                    "preference": preference.preference_id,
                },
                user_id=user[data_source.EMAIL],
            )
            continue

        if preference.method == "LIVE":
            for device in preference.devices:
                send_to_device(publisher, message, device, user[data_source.EMAIL])
                audit_notification(
                    message[OutputMessageKeys.ID],
                    {
                        "event": "Sent to consumer",
                        "method": preference.method,
                        "device": device.type,
                        "subtype": device.subtype if hasattr(device, "subtype") else "",
                        "token": device.token,
                        "status": device.status if hasattr(device, "status") else "",
                        "preference": preference.preference_id,
                    },
                    user_id=user[data_source.EMAIL],
                )
            continue

        logging.error("Invalid delivery method: %s", preference.method)


def send_to_device(publisher: megabus.Publisher, message: Dict, device: Device, email: str):
    """Specific implementation of send to device.

    :param publisher: Publisher object used to publish messages
    :param device: Device data
    :param message: message object
    :param email: e-mail
    """
    logging.debug("Device target: %s", device.name)
    if device.type == "MAIL":
        send_live_email(publisher, message, device, email)
        return

    if device.type == "PHONE" and device.subType == "MAIL2SMS":
        send_live_sms(publisher, message, device, email)
        return

    if device.type == "BROWSER":
        if device.subType == "OTHER":
            logging.info(device)
            send_live_webpush(publisher, message, device, email)
            return

        if device.subType == "SAFARI":
            logging.info(device)
            send_live_safaripush(publisher, message, device, email)
            return

    if device.type == "APP":
        if device.subType == "WINDOWS":
            send_live_webpush(publisher, message, device, email, encoding="aesgcm")
            return
        if device.subType == "MATTERMOST":
            send_live_mattermost(publisher, message, device, email)
            return

        # if device.subType == "LINUX":
        #     send_live_webpush(publisher, message, device, email)
        #     return
        #
        # if device.subType == "MAC":
        #     send_live_webpush(publisher, message, device, email)
        #     return
        #
        # if device.subType == "ANDROID":
        #     send_live_webpush(publisher, message, device, email)
        #     return
        #
        # if device.subType == "IOS":
        #     send_live_webpush(publisher, message, device, email)
        #     return

    logging.error("Invalid delivery device type: %s", device.type)


def apply_all(
    publisher: megabus.Publisher,
    message: Dict,
    email: str,
    user_devices: Dict[str, List[Device]],
):
    """Specific implementation to send to all devices for Critical notifications.

    :param publisher: Publisher object used to publish messages
    :param message: message object
    :param email: e-mail
    :param user_devices: all devices of the user
    """
    logging.debug("Applying to all types/devices")
    for device in user_devices[DataSource.DEVICES]:
        send_to_device(publisher, message, device, email)
        audit_notification(
            message[OutputMessageKeys.ID],
            {
                "event": "Critical sent to consumer",
                "device": device.type,
                "subtype": device.subtype if hasattr(device, "subtype") else "",
                "token": device.token,
                "status": device.status if hasattr(device, "status") else ""
            },
            user_id=email,
        )


def apply_default_preferences(publisher: megabus.Publisher, message: Dict, email: str):
    """Specific implementation of apply default preferences.

    :param publisher: Publisher object used to publish messages
    :param message: message object
    :param email: e-mail
    :param user_devices: all devices of the user
    """
    logging.debug("Applying default preference")

    # No device, defaulting to user's email
    device = Device()
    device.token = email

    send_live_email(publisher, message, device, email)
    audit_notification(
        message[OutputMessageKeys.ID],
        {"event": "Default sent to consumer", "device": "MAIL", "token": email},
        user_id=email,
    )


def send_live_sms(publisher: Publisher, message: Dict, device: Device, email: str):
    """Send live e-mail that targets phone.

    :param publisher: Publisher object used to publish messages
    :param message: message object
    :param device: device object
    :param email: e-mail
    """
    logging.debug("Sending to mail2sms:\n\t summary:%s \n\t email:%s", message[OutputMessageKeys.SUMMARY], email)
    message = convert_notifications_sms_to_json_string(message, device, email)
    publisher.send(message, extension="mail2sms", ttl=Config.TTL, headers={"persistent": "true"})


def send_live_email(publisher: Publisher, message: Dict, device: Device, email: str):
    """Send live e-mail.

    :param publisher: Publisher object used to publish messages
    :param message: message object
    :param device: device object
    :param email: e-mail
    """
    logging.debug("Sending to mail:\n\t summary:%s \n\t email:%s", message[OutputMessageKeys.SUMMARY], email)
    message = convert_notification_email_to_json_string(message, device, email)
    publisher.send(message, extension="email", ttl=Config.TTL, headers={"persistent": "true"})


def send_live_webpush(publisher: Publisher, message: Dict, device: Device, email: str, **kwargs):
    """Send live browser web push notification.

    :param publisher: Publisher object used to publish messages
    :param message: message object
    :param device: device object
    :param email: user's email
    :param encoding: allows to specify aesgcm crypto insteadn of default aes128gcm (mainly for Windows UWP test)
    """
    encoding = kwargs.get("encoding", None)
    logging.debug("Sending to webpush:\n\tsummary: %s\n\ttoken: %s", message[OutputMessageKeys.SUMMARY], device.token)
    message = convert_notification_push_to_json_string(message, device, email, encoding)
    publisher.send(message, extension="webpush", ttl=Config.TTL, headers={"persistent": "true"})


def send_live_safaripush(publisher: Publisher, message: Dict, device: Device, email: str):
    """Send live safari web push notification.

    :param publisher: Publisher object used to publish messages
    :param message: message object
    :param device: device object
    :param email: user's email
    """
    logging.debug("Sending to safari:\n\tsummary: %s\n\ttoken: %s", message[OutputMessageKeys.SUMMARY], device.token)
    message = convert_notification_push_to_json_string(message, device, email)
    publisher.send(message, extension="safaripush", ttl=Config.TTL, headers={"persistent": "true"})


def send_live_mattermost(publisher: Publisher, message: Dict, device: Device, email: str, **kwargs):
    """Send live Mattermost notification.

    :param publisher: Publisher object used to publish messages
    :param message: message object
    :param device: device object
    :param email: user's email
    """
    logging.debug("Sending to MM:\n\tsummary: %s\n\ttoken: %s", message[OutputMessageKeys.SUMMARY], device.token)
    message = convert_notification_push_to_json_string(message, device, email)
    publisher.send(message, extension="mattermost", ttl=Config.TTL, headers={"persistent": "true"})
