"""Authorization Service handling."""
import json
import logging
from typing import Dict, Optional

import requests

from notifications_routing.config import Config
from notifications_routing.data_source.data_source import DataSource

from .exceptions import BadResponseCodeError


def get_auth_access_token():
    """Return the Bearer access token."""
    url = Config.CERN_ACCESS_TOKEN_URL
    headers = Config.CERN_ACCESS_TOKEN_HEADERS
    data = Config.CERN_ACCESS_TOKEN_DATA
    response = requests.post(url, headers=headers, data=data)

    token = response.json().get("access_token")

    return token


def _get_group_users(group_id: str, headers: Dict[str, str], url: str = None):
    if not url:
        url = f"{Config.CERN_GROUP_URL}/{group_id}/{Config.CERN_GROUP_QUERY}"

    url = f"{Config.CERN_AUTH_SERVICE_URL}{url}"
    r = requests.get(url, headers=headers)
    if r.status_code != requests.codes.ok and r.status_code != requests.codes.not_found:
        raise BadResponseCodeError(url, status_code=r.status_code)

    return json.loads(r.content)


def get_group_users_api(group_id: str):
    """Get members of a group."""
    token = get_auth_access_token()
    headers = {"Authorization": "Bearer {}".format(token)}

    response = _get_group_users(group_id, headers)
    if "data" not in response:
        return []

    data = response["data"]

    while response.get("pagination").get("next"):
        response = _get_group_users(group_id, headers, response["pagination"]["next"])
        data.extend(response["data"])

    group_users = []
    for member in data:
        user = _prepare_user(member)

        # Should Audit failures to failed_targets
        if user:
            group_users.append(user)

    return group_users


def _prepare_user(member: Dict[str, str]) -> Optional[Dict[str, str]]:
    """Prepare user data (username and email) according to type and available fields."""
    if member["type"] == "Application":
        return None

    if member["unconfirmed"]:
        return {DataSource.USERNAME: member["unconfirmedEmail"], DataSource.EMAIL: member["unconfirmedEmail"]}

    # always respect "activeUser"
    # activeUser field is omitted in Application and Service types and Person type from external sources
    if "activeUser" in member and member["activeUser"] is False:
        if member["externalEmail"]:
            return {DataSource.USERNAME: member["upn"], DataSource.EMAIL: member["externalEmail"]}
        # member is not active and we don't active an externalEmail
        return None

    if member["primaryAccountEmail"]:
        return {DataSource.USERNAME: member["upn"], DataSource.EMAIL: member["primaryAccountEmail"]}

    # some secondary accounts don't have primaryAccountEmail
    if member["type"] == "Secondary":
        return {DataSource.USERNAME: member["upn"], DataSource.EMAIL: member["upn"] + "@cern.ch"}

    logging.error("Could not extract user info: %s", member)
    return None
