"""Notifications consumer app definition and creation."""
import asyncio
import logging
import logging.config

import yaml
from megabus import Consumer, Publisher

from notifications_routing.config import load_config
from notifications_routing.data_source.postgres.postgres_data_source import PostgresDataSource
from notifications_routing.router import Router


class App:
    """Notifications consumer app."""

    def __init__(self, config):
        """Initialize the App."""
        self.config = config

    async def connect(self):
        """Init all dependencies and start the listener."""
        data_source = PostgresDataSource()
        publisher = Publisher(self.config.PUBLISHER_NAME)
        listener = Router(publisher, data_source)

        cons = Consumer(
            self.config.CONSUMER_NAME,
            auto_connect=False,
            listener=listener,
        )
        listener.set_connections(cons._connections)
        cons.connect()

        logging.info("Finished initialising - Waiting for messages...")

    def run(self):
        """Run the app in a loop."""
        loop = asyncio.get_event_loop()
        loop.run_until_complete(self.connect())
        loop.run_forever()


def configure_logging(config):
    """Configure logs."""
    with open(config.LOGGING_CONFIG_FILE, "rt") as file:
        config = yaml.safe_load(file.read())
        logging.config.dictConfig(config)


def create_app():
    """Create a new App."""
    config = load_config()
    if config.SENTRY_DSN:
        import sentry_sdk

        sentry_sdk.init(dsn=config.SENTRY_DSN)

    configure_logging(config)

    return App(config)


def run():
    """Run App."""
    app = create_app()
    app.run()
