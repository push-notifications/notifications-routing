"""Postgres Data Source Implementation."""
import logging
import uuid
from contextlib import contextmanager
from datetime import datetime, time
from typing import Dict, List

from psycopg2 import errors
from sqlalchemy import (
    Boolean,
    Column,
    Date,
    DateTime,
    ForeignKey,
    Integer,
    MetaData,
    String,
    Table,
    Time,
    and_,
    create_engine,
    or_,
)
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.exc import IntegrityError
from sqlalchemy.ext.automap import automap_base
from sqlalchemy.orm import relationship, sessionmaker
from sqlalchemy.orm.exc import MultipleResultsFound

from notifications_routing.authorization_service import get_group_users_api
from notifications_routing.config import Config
from notifications_routing.data_source.data_source import DataSource
from notifications_routing.data_source.data_source import Device as DeviceModel
from notifications_routing.data_source.data_source import Preference as PreferenceModel
from notifications_routing.exceptions import DuplicatedError, MultipleResultsFoundError, NotFoundDataSourceError
from notifications_routing.utils import FeedFrequency


class PostgresDataSource(DataSource):
    """Implements methods from DataSource interface."""

    Base = automap_base(
        metadata=MetaData(
            schema=Config.DB_SCHEMA,
        )
    )

    def __init__(self):
        """Initialize Data Source."""
        logging.debug("Init PostgresDataSource")
        self.__engine = create_engine(Config.SQLALCHEMY_DATABASE_URI)
        self.__session = sessionmaker(self.__engine)
        self.Base.prepare(self.__engine)

    @staticmethod
    def __build_user(user: "User") -> Dict[str, str]:
        """Convert a user from the database model to a user dict.

        :param user: User
        :return: Dict containing user_id, username, email and last_login
        """
        return {
            DataSource.USER_ID: user.id,
            DataSource.USERNAME: user.username,
            DataSource.EMAIL: user.email,
            DataSource.LAST_LOGIN: user.lastLogin,
        }

    @contextmanager
    def session(self):
        """Open Session With Database."""
        session = self.__session()
        session.expire_on_commit = False

        try:
            yield session
            session.commit()
        except Exception:
            session.rollback()
            raise
        finally:
            session.close()

    def __get_scalar(self, session, model, **kwargs):
        """Query the model based on kwargs and returns the first matching element.

        :raises: MultipleResultsFoundError
        """
        try:
            return session.query(model).filter_by(**kwargs).scalar()
        except MultipleResultsFound:
            raise MultipleResultsFoundError(model.__tablename__, **kwargs)

    def get_channel_subscribed_users(self, channel_id: str, **kwargs) -> List[Dict[str, str]]:
        """Return the list of users subscribed to a channel.

        :param channel_id: Channel UUID
        :return: A list of user data in the format of dicts containing user_id, username, email and last_login
        :raises: NotFoundDataSourceError, MultipleResultsFoundError
        """
        with self.session() as session:
            channel = self.__get_scalar(session, Channel, id=channel_id, deleteDate=None)
            if not channel:
                raise NotFoundDataSourceError(Channel, id=channel_id)

            unsubscribed_ids = [user.id for user in channel.unsubscribed]
            return [self.__build_user(member) for member in channel.members if member.id not in unsubscribed_ids]

    def get_target_users(self, notification_id: str, **kwargs) -> List[Dict[str, str]]:
        """Return the list of users targeted by a notification.

        :param notification_id: Notification UUID
        :return: A list of user data in the format of dicts containing user_id, username, email and last_login
        :raises: NotFoundDataSourceError, MultipleResultsFoundError
        """
        with self.session() as session:
            notification = self.__get_scalar(session, Notification, id=notification_id)
            if not notification:
                raise NotFoundDataSourceError(Notification, id=notification_id)

            return [self.__build_user(member) for member in notification.target_users]

    def get_target_groups(self, notification_id: str, **kwargs) -> List[str]:
        """Return the list of groups targeted to a notification.

        :param notification_id: Notification UUID
        :return: A list of group's uuids
        :raises: NotFoundDataSourceError, MultipleResultsFoundError
        """
        with self.session() as session:
            notification = self.__get_scalar(session, Notification, id=notification_id)
            if not notification:
                raise NotFoundDataSourceError(Notification, id=notification_id)

            return [group.id for group in notification.target_groups]

    def get_channel_unsubscribed_users(self, channel_id: str, **kwargs) -> List[str]:
        """Return the list of users unsubscribed to a channel.

        :param channel_id: Channel UUID
        :return: A list of user's usernames
        :raises: NotFoundDataSourceError, MultipleResultsFoundError
        """
        with self.session() as session:
            channel = self.__get_scalar(session, Channel, id=channel_id, deleteDate=None)
            if not channel:
                raise NotFoundDataSourceError(Channel, id=channel_id)

            return [unsubscribed.username for unsubscribed in channel.unsubscribed]

    def get_channel_groups(self, channel_id: str, **kwargs) -> List[str]:
        """Return the list of groups members of a channel.

        :param channel_id: Channel UUID
        :return: A list of group's uuids
        :raises: NotFoundDataSourceError, MultipleResultsFoundError
        """
        with self.session() as session:
            channel = self.__get_scalar(session, Channel, id=channel_id, deleteDate=None)
            if not channel:
                raise NotFoundDataSourceError(Channel, id=channel_id)

            return [group.id for group in channel.groups]

    def get_group_users(self, group_id: str, **kwargs) -> List[Dict[str, str]]:
        """Return the list of users that belong to a group.

        :param group_id: Group UUID or ID
        :return: A list of user data in the format of dicts containing username and email
        :raises: BadResponseCodeError
        """
        return get_group_users_api(group_id)

    def get_user_preferences(self, user_id: str, channel_id: str, **kwargs) -> Dict[str, List["Preference"]]:
        """Return the list of user preferences.

        :param user_id: User UUID
        :param channel_id: Channel UUID
        :return: A dict containing the user email and a list of preferences
        :raises: NotFoundDataSourceError, MultipleResultsFoundError
        """
        with self.session() as session:
            user = self.__get_scalar(session, User, id=user_id)
            if not user:
                raise NotFoundDataSourceError(User, id=user_id)

            preferences = (
                session.query(Preference)
                .filter(
                    Preference.userId == user.id,
                    or_(
                        Preference.targetId == channel_id,
                        Preference.targetId.is_(None),
                    ),
                    ~Preference.disabledChannels.any(Channel.id == channel_id),
                )
                .all()
            )

            # TODO: refactor this to just return a list of preferences object with the fields needed
            return {DataSource.EMAIL: user.email, DataSource.PREFERENCES: preferences}

    def get_user_devices(self, user_id: str, **kwargs) -> Dict[str, List["Device"]]:
        """Return the list of user devices.

        :param user_id: User UUID
        :return: A dict containing the user email and a list of devices
        :raises: NotFoundDataSourceError, MultipleResultsFoundError
        """
        with self.session() as session:
            user = self.__get_scalar(session, User, id=user_id)

            if not user:
                raise NotFoundDataSourceError(User, id=user_id)

            devices = (
                session.query(Device)
                .filter(
                    Device.userId == user.id,
                )
                .all()
            )

            # TODO: refactor this to just return a list of devices object with the fields needed
            user_devices = {DataSource.EMAIL: user.email, DataSource.DEVICES: devices}

            return user_devices

    def is_user_mute_active(self, user_id: str, channel_id: str = None, **kwargs) -> bool:
        """Return true if the user has an active mute for the specified channel or globally.

        :param user_id: User UUID
        :param channel_id: Channel UUID. Optional.
        :return: Bool
        :raises: NotFoundDataSourceError, MultipleResultsFoundError
        """
        with self.session() as session:
            user = self.__get_scalar(session, User, id=user_id)
            if not user:
                raise NotFoundDataSourceError(User, id=user_id)

            return session.query(
                session.query(Mute)
                .filter(
                    Mute.userId == user.id,
                    or_(
                        Mute.targetId == channel_id,
                        Mute.targetId.is_(None),
                    ),
                    or_(
                        Mute.type == "PERMANENT",
                        and_(
                            Mute.type == "RANGE",
                            Mute.end > datetime.now(),
                            or_(
                                Mute.start.is_(None),
                                Mute.start < datetime.now(),
                            ),
                        ),
                    ),
                )
                .exists()
            ).scalar()

    def create_feed_user_notification(self, user_feed_notification: "UserFeedNotification"):
        """Save a FeedUserNotification Object to Database.

        :param user_feed_notification: UserFeedNotification
        :raises: DuplicatedError
        """
        try:
            with self.session() as session:
                session.add(user_feed_notification)
        except IntegrityError as e:
            if isinstance(e.orig, errors.UniqueViolation):
                raise DuplicatedError(UserFeedNotification.__tablename__, e)
            else:
                raise

    def get_unique_users_from_feed_notifications(
        self,
        runtime_datetime: datetime,
        runtime_time: time,
        runtime_day_of_week: int,
        frequency_type: FeedFrequency,
    ) -> List[UUID]:
        """Return unique users from feed notifications for specific delivery datetime and frequency.

        :param runtime_datetime: Runtime Datetime
        :param runtime_time: Runtime Time
        :param runtime_day_of_week: Day of week as an integer
        :param frequency_type: Frequency type

        :return List of users uuids
        """
        with self.session() as session:
            user_ids = session.query(UserFeedNotification.userId).filter(
                UserFeedNotification.frequencyType == frequency_type,
                UserFeedNotification.creationDatetime <= runtime_datetime,
                UserFeedNotification.deliveryTime == runtime_time,
            )

            if not frequency_type == FeedFrequency.DAILY:
                user_ids = user_ids.filter(UserFeedNotification.deliveryDayOfTheWeek == runtime_day_of_week)

            # TODO: process in batches
            return user_ids.distinct()  # .yield_per(Config.QUERY_BATCH_SIZE)

    def get_user_feed_notifications_ids(
        self,
        user_id: str,
        runtime_datetime: datetime,
        runtime_time: time,
        runtime_day_of_week: int,
        frequency_type: FeedFrequency,
    ) -> List[str]:
        """Return user feed notifications ids for specific delivery datetime and frequency.

        :param user_id: User UUID
        :param runtime_datetime: Runtime Datetime
        :param runtime_time: Runtime Time
        :param runtime_day_of_week: Day of week as an integer
        :param frequency_type: Frequency type

        :return List of notifications ids as strings
        """
        with self.session() as session:
            ids = session.query(UserFeedNotification.notificationId).filter(
                UserFeedNotification.userId == user_id,
                UserFeedNotification.frequencyType == frequency_type,
                UserFeedNotification.creationDatetime <= runtime_datetime,
                UserFeedNotification.deliveryTime == runtime_time,
            )

            if not frequency_type == FeedFrequency.DAILY:
                ids = ids.filter(UserFeedNotification.deliveryDayOfTheWeek == runtime_day_of_week)

            return [str(notificationId) for notificationId, in ids]

    def delete_user_feed_notifications(
        self,
        user_id: UUID,
        runtime_datetime: datetime,
        runtime_time: time,
        runtime_day_of_week: int,
        frequency_type: FeedFrequency,
    ) -> None:
        """Delete user feed notifications.

        :param user_id: User UUID
        :param runtime_datetime: Runtime Datetime
        :param runtime_time: Runtime Time
        :param runtime_day_of_week: Day of week as an integer
        :param frequency_type: Frequency type

        :return None
        """
        with self.session() as session:
            user_feed_notifications = session.query(UserFeedNotification).filter(
                UserFeedNotification.userId == user_id,
                UserFeedNotification.frequencyType == frequency_type,
                UserFeedNotification.creationDatetime <= runtime_datetime,
                UserFeedNotification.deliveryTime == runtime_time,
            )

            if not frequency_type == FeedFrequency.DAILY:
                user_feed_notifications = user_feed_notifications.filter(
                    UserFeedNotification.deliveryDayOfTheWeek == runtime_day_of_week
                )

            return user_feed_notifications.delete()

    def get_system_user(self, username: str, **kwargs) -> Dict[str, str]:
        """Return user if in system.

        :param username: User UUID
        :return Dict of user data containing user_id, username, email and last_login
        :raises: NotFoundDataSourceError, MultipleResultsFoundError
        """
        with self.session() as session:
            user = self.__get_scalar(session, User, username=username)
            if not user:
                raise NotFoundDataSourceError(User, username=username)

            return {
                DataSource.USER_ID: user.id,
                DataSource.USERNAME: user.username,
                DataSource.EMAIL: user.email,
                DataSource.LAST_LOGIN: user.lastLogin,
            }


class User(PostgresDataSource.Base):
    """User Model."""

    __tablename__ = "Users"

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    username = Column(String)
    email = Column(String)
    enabled = Column(Boolean)
    created = Column(DateTime)
    lastLogin = Column(DateTime)
    preferences = relationship("Preference")
    devices = relationship("Device")


channel_groups = Table(
    "channels_groups__groups",
    PostgresDataSource.Base.metadata,
    Column("channelsId", UUID(as_uuid=True), ForeignKey("Channels.id")),
    Column("groupsId", UUID(as_uuid=True), ForeignKey("Groups.id")),
)

channel_members = Table(
    "channels_members__users",
    PostgresDataSource.Base.metadata,
    Column("channelsId", UUID(as_uuid=True), ForeignKey("Channels.id")),
    Column("usersId", UUID(as_uuid=True), ForeignKey("Users.id")),
)

channel_unsubscribed = Table(
    "channels_unsubscribed__users",
    PostgresDataSource.Base.metadata,
    Column("channelsId", UUID(as_uuid=True), ForeignKey("Channels.id")),
    Column("usersId", UUID(as_uuid=True), ForeignKey("Users.id")),
)

notification_target_users = Table(
    "notifications_target_users__users",
    PostgresDataSource.Base.metadata,
    Column("notificationsId", UUID(as_uuid=True), ForeignKey("Notifications.id")),
    Column("usersId", UUID(as_uuid=True), ForeignKey("Users.id")),
)

notification_target_groups = Table(
    "notifications_target_groups__groups",
    PostgresDataSource.Base.metadata,
    Column("notificationsId", UUID(as_uuid=True), ForeignKey("Notifications.id")),
    Column("groupsId", UUID(as_uuid=True), ForeignKey("Groups.id")),
)

preferences_devices = Table(
    "preferences_devices__devices",
    PostgresDataSource.Base.metadata,
    Column("preferencesId", UUID(as_uuid=True), ForeignKey("Preferences.id")),
    Column("devicesId", UUID(as_uuid=True), ForeignKey("Devices.id")),
)

preferences_disabled_channels = Table(
    "preferences_disabled_channels__channels",
    PostgresDataSource.Base.metadata,
    Column("preferencesId", UUID(as_uuid=True), ForeignKey("Preferences.id")),
    Column("channelsId", UUID(as_uuid=True), ForeignKey("Channels.id")),
)


class Device(PostgresDataSource.Base, DeviceModel):
    """Device Model."""

    __tablename__ = "Devices"

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    name = Column(String)
    userId = Column(UUID(as_uuid=True), ForeignKey("Users.id"))
    info = Column(String)
    type = Column(String)
    subType = Column(String)
    token = Column(String)
    status = Column(String)


class Preference(PostgresDataSource.Base, PreferenceModel):
    """Preference Model."""

    __tablename__ = "Preferences"

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    type = Column(String)
    name = Column(String)
    userId = Column(UUID(as_uuid=True), ForeignKey("Users.id"))
    targetId = Column(String)
    notificationPriority = Column(String)
    rangeStart = Column(Time)
    rangeEnd = Column(Time)
    scheduledTime = Column(Time)
    scheduledDay = Column(Integer)
    devices = relationship("Device", secondary=preferences_devices, lazy="subquery")
    disabledChannels = relationship("Channel", secondary=preferences_disabled_channels, lazy="subquery")


class Mute(PostgresDataSource.Base):
    """Mute Model."""

    __tablename__ = "Mutes"

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    type = Column(String)
    userId = Column(UUID(as_uuid=True), ForeignKey("Users.id"))
    targetId = Column(String)
    start = Column(DateTime)
    end = Column(DateTime)


class Channel(PostgresDataSource.Base):
    """Channel Model."""

    __tablename__ = "Channels"

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    slug = Column(String)
    name = Column(String)
    groups = relationship("Group", secondary=channel_groups)
    members = relationship("User", secondary=channel_members)
    deleteDate = Column(Date)
    unsubscribed = relationship("User", secondary=channel_unsubscribed)


class Group(PostgresDataSource.Base):
    """Group Model."""

    __tablename__ = "Groups"

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    groupIdentifier = Column(String)


class Notification(PostgresDataSource.Base):
    """Notification Model."""

    __tablename__ = "Notifications"
    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)

    target_users = relationship("User", secondary=notification_target_users)
    target_groups = relationship("Group", secondary=notification_target_groups)


class UserFeedNotification(PostgresDataSource.Base):
    """UserDailyNotification Model."""

    __tablename__ = "UserFeedNotifications"

    userId = Column(UUID(as_uuid=True), ForeignKey("Users.id"), primary_key=True)
    notificationId = Column(UUID(as_uuid=True), ForeignKey("Notifications.id"), primary_key=True)
    frequencyType = Column(String, primary_key=True)
    creationDatetime = Column(DateTime, index=True)
    deliveryTime = Column(Time, nullable=True)
    deliveryDayOfTheWeek = Column(Integer, nullable=True)

    def __init__(
        self,
        user_id,
        notification_id,
        notification_frequency_type,
        notification_creation_datetime,
        notification_delivery_time,
        notification_delivery_day_of_the_week,
    ) -> None:
        """Initialize UserDailyNotification."""
        super().__init__()
        self.userId = user_id
        self.notificationId = notification_id
        self.frequencyType = notification_frequency_type
        self.creationDatetime = notification_creation_datetime
        self.deliveryTime = notification_delivery_time
        self.deliveryDayOfTheWeek = notification_delivery_day_of_the_week
