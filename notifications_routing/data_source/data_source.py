"""Abstract Data Source."""
from abc import ABC, abstractmethod
from datetime import datetime, time
from typing import Dict, List

from notifications_routing.utils import FeedFrequency


class DataSource(ABC):
    """Generic data source interface."""

    # Keys for DataSource responses
    USER_ID = "user_id"
    USERNAME = "username"
    EMAIL = "email"
    GROUP_ID = "group_id"
    PREFERENCES = "preferences"
    DEVICES = "devices"
    LAST_LOGIN = "last_login"

    @abstractmethod
    def get_channel_subscribed_users(self, channel_id: str, **kwargs) -> List[Dict[str, str]]:
        """Return the list of users subscribed to a channel.

        :param channel_id: Channel UUID
        :return: A list of user data in the format of dicts containing user id, username, email and last login
        :raises: NotFoundDataSourceError, MultipleResultsFoundError
        """
        pass

    @abstractmethod
    def get_channel_groups(self, channel_id: str, **kwargs) -> List[str]:
        """Return the list of groups members of a channel.

        :param channel_id: Channel UUID
        :return: A list of group's uuids
        :raises: NotFoundDataSourceError, MultipleResultsFoundError
        """
        pass

    @abstractmethod
    def get_group_users(self, group_id: str, **kwargs) -> List[Dict[str, str]]:
        """Return the list of users that belong to a group.

        :param group_id: Group UUID or ID
        :return: A list of user data in the format of dicts containing username and email
        :raises: BadResponseCodeError
        """
        pass

    @abstractmethod
    def get_user_preferences(self, user_id: str, channel_id: str, **kwargs) -> Dict[str, List["Preference"]]:
        """Return the list of user preferences.

        :param user_id: User UUID
        :param channel_id: Channel UUID
        :return: A dict containing the user email and a list of preferences
        :raises: NotFoundDataSourceError, MultipleResultsFoundError
        """
        pass

    @abstractmethod
    def get_user_devices(self, user_id: str, **kwargs) -> Dict[str, List["Device"]]:
        """Return the list of user devices.

        :param user_id: User UUID
        :return: A dict containing the user email and a list of devices
        :raises: NotFoundDataSourceError, MultipleResultsFoundError
        """
        pass

    @abstractmethod
    def is_user_mute_active(self, user_id: str, channel_id: str, **kwargs) -> bool:
        """Return true if the user has an active mute for the specified channel or globally.

        :param user_id: User UUID
        :param channel_id: Channel UUID. Optional.
        :return: Bool
        :raises: NotFoundDataSourceError, MultipleResultsFoundError
        """
        pass

    @abstractmethod
    def create_feed_user_notification(self, user_feed_notification: "UserFeedNotification") -> None:
        """Save a FeedUserNotification Object to Database.

        :param user_feed_notification: UserFeedNotification
        :raises: DuplicatedError
        """
        pass

    @abstractmethod
    def get_unique_users_from_feed_notifications(
        self, runtime_datetime: datetime, runtime_time: time, runtime_day_of_week: int, frequency_type: FeedFrequency
    ) -> List[str]:
        """Return unique users from feed notifications for specific delivery datetime and frequency.

        :param runtime_datetime: Runtime Datetime
        :param runtime_time: Runtime Time
        :param runtime_day_of_week: Day of week as an integer
        :param frequency_type: Frequency type

        :return List of users uuids
        """
        pass

    @abstractmethod
    def get_user_feed_notifications_ids(
        self,
        user_id: str,
        runtime_datetime: datetime,
        runtime_time: time,
        runtime_day_of_week: int,
        frequency_type: FeedFrequency,
    ) -> List[str]:
        """Specific implementation of get user feed notifications ids for specific delivery datetime and frequency.

        :param user_id: User id
        :param runtime_datetime: Datetime
        :param runtime_time: Time
        :param runtime_day_of_week: day of the week
        :param frequency_type: frequency type
        """
        pass

    @abstractmethod
    def delete_user_feed_notifications(
        self,
        user_id: str,
        runtime_datetime: datetime,
        runtime_time: time,
        runtime_day_of_week: int,
        frequency_type: FeedFrequency,
    ) -> None:
        """Specific implementation of delete user feed notifications.

        :param user_id: User id
        :param runtime_datetime: Datetime
        :param runtime_time: Time
        :param runtime_day_of_week: day of the week
        :param frequency_type: frequency type
        """
        pass

    @abstractmethod
    def get_system_user(self, username: str, **kwargs) -> Dict[str, str]:
        """Return user if in system.

        :param username: User UUID
        :return Dict of user data containing user_id, username, email and last_login
        :raises: NotFoundDataSourceError, MultipleResultsFoundError
        """
        pass

    @abstractmethod
    def get_channel_unsubscribed_users(self, channel_id: str, **kwargs) -> List[str]:
        """Return the list of users unsubscribed to a channel.

        :param channel_id: Channel UUID
        :return: A list of user's usernames
        :raises: NotFoundDataSourceError, MultipleResultsFoundError
        """
        pass

    @abstractmethod
    def get_target_users(self, notification_id: str, **kwargs) -> List[Dict[str, str]]:
        """Return the list of users targeted by a notification.

        :param notification_id: Notification UUID
        :return: A list of user data in the format of dicts containing user_id, username, email and last_login
        :raises: NotFoundDataSourceError, MultipleResultsFoundError
        """
        pass

    @abstractmethod
    def get_target_groups(self, notification_id: str, **kwargs) -> List[str]:
        """Return the list of groups targeted to a notification.

        :param notification_id: Notification UUID
        :return: A list of group's uuids
        :raises: NotFoundDataSourceError, MultipleResultsFoundError
        """
        pass


class Preference:
    """Preference Model."""

    pass


class Device:
    """Device Model."""

    pass


class UserFeedNotification:
    """UserFeedNotification Model."""

    pass
