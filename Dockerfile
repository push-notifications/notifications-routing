FROM gitlab-registry.cern.ch/push-notifications/notifications-routing/notifications-routing-base:9ae84809
ARG build_env

COPY ./ ./

RUN python -V && \
    pip -V && \
    pip install --upgrade pip && \
    pip install poetry && \
    poetry config virtualenvs.create false

RUN if [ "$build_env" == "development" ]; \
    then poetry install; \
    else poetry install --no-dev; \
    fi

CMD ["python",  "-m", "notifications_routing"]
